var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

var fromScss = './scss/';
var toCss = './build/';
var fileCss = 'customizer.bundle.css';

gulp.task("css", function () {
    return gulp
        .src([
            fromScss + "*.scss",
        ])
        .pipe(
            sass
                .sync({
                    outputStyle: "expanded"
                })
                .on("error", sass.logError)
        )
        .pipe(concat(fileCss))
        .pipe(gulp.dest(toCss))
    // .pipe(browserSync.stream());
});

gulp.task('watch', function compileCss(done) {
    gulp.watch([
        fromScss,
    ],
        gulp.series("css")
    );
    done();
});
