const WEBSITE_CONFIG = {
    lowersCanvasHeight : [ // put all elements that influence canvas height and subtract their height from 100vh
        '#site-header',
        '.top-banner-container',
    ],
    lowersCanvasHeightMobile: [
        '#site-header',
        '.top-banner-container',
        '#productTitle',
        '#additionalMarginOnMobile',
    ]
};

const EXPORTER_CONFIG = [

    {
        product: 'wine1',
        config : [
        ],
    },

]; 

const UI_CONFIG = {
    
}

const PRODUCT_LIST = {
    
    'WINE01' : {
        model: {
            name: 'wine-bottle',
            baseColor: 'ffffff',
            opacity : 0.4,
        },
        upload : { /* requirements for upload */
            min: {
                width: 750,
                height: 675,
            }
        },
        parts : [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    {
                        type: 'label-default',
                        // value: 'label-test5.png',
                        key: 'front-label',
                        default: 'front-texture',
                        config: {
                            // SIZE of label
                            width: 750,
                            height: 675,
                            // POSITION of label
                            offsetX: 137,
                            offsetY: 225,
                        }
                    },
                    {
                        type: 'image',
                        value: 'default-label-with-text.png',
                        key: 'front-texture',
                        config: {
                            // positioning? scaling?
                        }
                    },

                    // {
                    //     type: 'label',
                    //     value: 'label-test4.png',
                    //     key: 'front-label2',
                    //     default: 'front-texture2',
                    //     config: {
                    //         // positioning? scaling?
                    //     }
                    // },
                    // {
                    //     type: 'image',
                    //     value: 'test3.jpg',
                    //     key: 'front-texture2',
                    //     config: {
                    //         // positioning? scaling?
                    //     }
                    // }
                ]
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'label',
                        value: 'label-test2.png',
                        key: 'back-label',
                        default: 'back-texture',
                        config: {
                            // positioning? scaling?
                        }
                    },
                    {
                        type: 'image',
                        value: 'default-label.png',
                        key: 'back-texture',
                        config: {
                            // positioning? scaling?
                        }
                    }
                ]
            },
            {
                name: 'Body', 
                key: 'body',
                layers: []
            }
        ],
        ui: [

            {
                key : 'front',
                group: [],
            },
            {
                ref: 'back'
            }
        ],
    },
};

