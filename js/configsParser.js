let ACTIVE_PRODUCT = {};

function getProductConfig(handle){
    let product = matchProduct(PRODUCT_LIST, handle);
    product.handle = handle.toLowerCase();
    ACTIVE_PRODUCT = product;
}

function matchProduct(list, handle){
    let product = null;
    for(let p in list){
        if( p.toLowerCase() == handle.toLowerCase() ){
            product = list[p];
            break;
        }
    }
    if(product === null) console.warn('PRODUCT not found!', handle, PRODUCTS);
    return product;
}

// if you have parameter id in url it will catch that, e.g. ?id=CC001
function localProductId(){
    if( ! window.location.href.includes('localhost')) return undefined;
    var url = new URL(window.location.href);
    return url.searchParams.get("id");
}

function getProductHandle(){
    return typeof PRODUCT_HANDLE !== 'undefined' ? PRODUCT_HANDLE.toLowerCase() : null;
}

(function(){
    getProductConfig( localProductId() || getProductHandle() || 'WINE01' );
})();