import { Component } from 'middleware/Component';
import { Payload } from 'middleware/Payload';
import { Config } from 'middleware/Config';
import { Action } from 'middleware/Action'; // TODO - rename mediator folder
import { ALL_CONFIGS_LOADED } from 'components/constants';
import { SceneLights } from 'components/lights/SceneLights';
import { WineScene } from 'components/scene/WineScene';
import { DebugCustomizer } from 'components/helpers/DebugCustomizer';
import { Product } from 'components/product/Product';
import { ProgressBar } from 'components/progress/ProgressBar';
import { StateManager } from 'components/manager/StateManager';
import { UI } from 'components/ui/UI';
import { SceneControls } from 'components/controls/SceneControls';
import { TestScene } from 'components/scene/TestScene';
import { LabelUI } from 'components/ui/LabelUI';
import { MaterialFacade } from 'components/material/MaterialFacade';
import { UiPlus3DFollow } from 'components/ui/UiPlus3DFollow';
 
export class Customizer{

    public static app: Customizer; // Customizer.app from outside
    public initiated : boolean = false;
    public busy : boolean = false;
    public action;
    public payload;
    public component;
    public config;

    public static run(params?){
        Customizer.instance();
        // let app = Customizer.instance();
        // if(app.busy) return;
        // app.wait();
        // if( app.initiated ) app.resume(params);
    }

    public static instance(): Customizer{
        if( ! Customizer.app ){
             const app = new Customizer();
        }
        return Customizer.app;
    }

    constructor(){

        Customizer.app = this;
        this.initMiddleware();
        this.initiated = true;
    }

    // all the components are triggered here
    protected async init(){

        window.scrollTo(0, 0);
      
        await this.component.add('progress-bar', new ProgressBar()); 
        await this.component.add('states', new StateManager()); 

        await this.component.add('scene', new WineScene() );
        await this.component.add('lights', new SceneLights() );
        await this.component.add('controls', new SceneControls() );

        await this.component.add('material', new MaterialFacade() );
        await this.component.add('product', new Product() );
       
        await this.component.add('ui', new UI() );
        await this.component.add('label', new LabelUI() );
        await this.component.add('plus3ddot', new UiPlus3DFollow() );

        await this.component.add('debug', new DebugCustomizer() );

        // at the end... preload everything
        this.payload.start();
    }

    protected initMiddleware(){
        this.action = this.initActions();
        this.config = this.initConfig();
        this.component = this.initComponents();
        this.payload = this.initPayload();
    }

    protected initActions(){
        return new Action([
            {
                name: ALL_CONFIGS_LOADED,
                run: this.init.bind(this),
            }
        ]);
    }

    protected initPayload(){
        return new Payload();
    }

    protected initComponents(){
        return new Component();
    }

    protected initConfig(){
        return new Config();
    }

    public wait(){
        this.busy = true;
        setTimeout(()=>{
            this.busy = false;
        }, 500);
    }

    // not needed because app will autoload on page load
    public resume(params?){

    }
    
    

}

window.addEventListener('load', ()=>{
    Customizer.run();
})

