import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { EVENT_GET_THREE_CAMERA, EVENT_GET_THREE_CANVAS, EVENT_ADD_TO_RENDERER, EVENT_GET_THREE_RENDERER, EVENT_TRIGGER_RENDER, EVENT_AFTER_CONTROLS_UPDATE, TRIGGER_MANUAL_CONTROLS_UPDATE, EVENT_CUSTOMIZER_SCENE_READY, EVENT_GET_CONTROLS, EVENT_MOUSE_PRESS, EVENT_MOUSE_RELEASE } from 'components/constants';
import { Customizer } from 'Customizer';
import { Math } from 'three';

export class SceneControls{
    

    public controls : OrbitControls;
    public controlsConfig;
    private onControlsUpdateFnList : any = [];

    constructor(){

        this.controlsConfig = this.getControlsConfig();

        Customizer.app.action.listen(EVENT_CUSTOMIZER_SCENE_READY, this.init.bind(this) );
        Customizer.app.action.listen(EVENT_TRIGGER_RENDER, this.triggerControlsUpdate.bind(this) );

        Customizer.app.action.register(EVENT_AFTER_CONTROLS_UPDATE, this.addFunctionAfterControlsUpdate.bind(this));
        Customizer.app.action.register(TRIGGER_MANUAL_CONTROLS_UPDATE, this.afterControlsUpdate.bind(this));
        Customizer.app.action.register(EVENT_GET_CONTROLS, this.getControls.bind(this));
    
    }

    public async init(){
        console.log('SceneControls INIT');
        this.controls = await this.createControls() as OrbitControls;
    }

    public getControls(){
        return this.controls;
    }

    protected getControlsConfig(){

        let config = {
            enabled: false,
			minDistance: 470,
			maxDistance: 800,
			minPolarAngle: Math.degToRad(60), // orbit vertically at top
			maxPolarAngle: Math.degToRad(120), // orbit vertically at bottom
            enableZoom: true,
            enablePan: false,
            enableDamping: true,
            dampingFactor: 0.15,
            rotateSpeed: 0.25,
            // autoRotate: false,
            // autoRotateSpeed: 0.15,
        }
        
        // if(window.innerWidth > 800){
            config.enabled = true;
        // }

        return config;
    }

    protected triggerControlsUpdate(){
        this.controls.update();
    }

    protected async createControls(){

        let camera = await Customizer.app.action.call(EVENT_GET_THREE_CAMERA, null, true) as THREE.Camera;
        let renderer = await Customizer.app.action.call(EVENT_GET_THREE_RENDERER, null, true) as THREE.Renderer;
        let canvas = await Customizer.app.action.call(EVENT_GET_THREE_CANVAS, null, true) as HTMLElement;

        const controls = new OrbitControls( camera, renderer.domElement );

        if(this.controlsConfig)
            for(let key in this.controlsConfig) controls[key] = this.controlsConfig[key];
        
        
        const toggleControlLock = (_toggle)=>{
            controls.autoRotate = _toggle;
        }

        controls.addEventListener('change', ()=>{
            this.afterControlsUpdate();
        })

        // TODO - where's mouseup and touchend located???
        canvas.addEventListener('mousedown', ()=>{
            Customizer.app.action.broadcast(EVENT_MOUSE_PRESS, null);
            return toggleControlLock(false);
        });
        window.addEventListener('mouseup', ()=>{
            Customizer.app.action.broadcast(EVENT_MOUSE_RELEASE, null);
        })
        canvas.addEventListener('touchstart', ()=>{
            Customizer.app.action.broadcast(EVENT_MOUSE_PRESS, null);
            return toggleControlLock(false);
        });
        window.addEventListener('touchend', ()=>{
            Customizer.app.action.broadcast(EVENT_MOUSE_RELEASE, null);
        })


        Customizer.app.action.call(EVENT_ADD_TO_RENDERER, null, null, 'update-orbit', this.triggerControlsUpdate.bind(this));

        
        return new Promise(resolve => resolve(controls));
    }

    protected addFunctionAfterControlsUpdate(...args){
        let fn = args[0];
        this.onControlsUpdateFnList.push( fn );
    }

    private afterControlsUpdate(){
        
        if(this.onControlsUpdateFnList.length > 0){
            for(let i=0;i<this.onControlsUpdateFnList.length;i++){
                this.onControlsUpdateFnList[i](this.controls);
            }
        }
    }

}