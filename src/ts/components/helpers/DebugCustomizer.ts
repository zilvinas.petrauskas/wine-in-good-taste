import { isLocalhost } from './helpers';
import { EVENT_GET_THREE_CAMERA, EVENT_GET_MODEL, EVENT_GET_CONTROLS } from 'components/constants';
import { Customizer } from 'Customizer';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class DebugCustomizer{
    
    constructor(){

        this.events();
    }

    private async events(){

        if( !isLocalhost()) return;

        window.addEventListener('keyup', (e) => {

            console.log(e.keyCode, e.code);
            
            if (e.keyCode === 67) { // [C]
                
               this.getCameraPos();
                
			} else if (e.keyCode === 69) { // [E]

                this.getModel();
            } else if (e.code === 'KeyK'){
                this.getControls();
            }
            
		});

    }

    private async getCameraPos(){

        const camera = await Customizer.app.action.call(EVENT_GET_THREE_CAMERA, null, null) as THREE.Camera;

        console.log('camera position', camera.position);
    }

    private async getModel(){

       let model = await Customizer.app.action.call(EVENT_GET_MODEL);
        console.log('model', model);
    }

    private async getControls(){
        const controls = await Customizer.app.action.call(EVENT_GET_CONTROLS, null, null) as OrbitControls;
        console.log('azimuth', controls.getAzimuthalAngle() );
        console.log('polar', controls.getPolarAngle() );
    }

}