export function SmoothScroll(_options){
    
    var scope = {} as any;
    var animationInterval;
    var defaultOptions = {
        speed: 500,
		speedAsDuration: false,
		durationMax: null,
		durationMin: null,
		clip: true,
        offset: 0,
        easing: 'easeInOutCubic',
        header: null,
    }
    var options = {...defaultOptions, ..._options};
    var fixedHeader;

    var cancelScroll = function () {
        cancelAnimationFrame(animationInterval);
        animationInterval = null;
    };

    var getCurrentPageY = function(){
        return window.innerWidth < 800 ? pageYOffset :  document.documentElement.scrollTop || document.body.scrollTop || window.pageYOffset; // Current location on the page
    }

    scope.to = function(scrollToY){

        var startLocation = getCurrentPageY();
        if (options.header && !fixedHeader) {
            // Get the fixed header if not already set
            fixedHeader = document.querySelector(options.header);
        }
        var headerHeight = getHeaderHeight(fixedHeader);
        var endLocation = scrollToY; // Location to scroll to
        var distance = endLocation - startLocation; // distance to travel
        var documentHeight = getDocumentHeight();
        var timeLapsed = 0;
        var speed = getSpeed(distance, options);
        var start, percentage, position;

        var stopAnimateScroll = function (position, endLocation) {

            // Get the current location
            var currentLocation = getCurrentPageY();

            // Check if the end location has been reached yet (or we've hit the end of the document)
            // if (position == endLocation || currentLocation == endLocation || ((startLocation < endLocation && window.innerHeight + currentLocation) >= documentHeight)) {
            if (position == endLocation || currentLocation == endLocation) {

                console.log('END POSITION', currentLocation);

                // focus
                document.body.focus();

                // Clear the animation timer
                cancelScroll();
                // Reset start
                start = null;
                animationInterval = null;
                return true;
            }

            return false;
        };
        

        /**
         * Loop scrolling animation
         */
        var loopAnimateScroll = function (timestamp) {
            if (!start) { start = timestamp; }
            timeLapsed += timestamp - start;
            percentage = speed === 0 ? 0 : (timeLapsed / speed);
            percentage = (percentage > 1) ? 1 : percentage;
            position = startLocation + (distance * easingPattern(options, percentage));
            window.scrollTo(0, Math.floor(position));
            console.log('pos', position);
            if (!stopAnimateScroll(position, endLocation)) {
                animationInterval = window.requestAnimationFrame(loopAnimateScroll);
                start = timestamp;
            }
        };

        /**
         * Reset position to fix weird iOS bug
         * @link https://github.com/cferdinandi/smooth-scroll/issues/45
         */
        if (window.pageYOffset === 0) {
            window.scrollTo(0, 0);
        }
        // Start scrolling animation
        cancelScroll();
        window.requestAnimationFrame(loopAnimateScroll);

    }

    var getHeaderHeight = function (header) {
		return !header ? 0 : (getHeight(header) + header.offsetTop);
    };
    var getHeight = function (elem) {
		return parseInt(window.getComputedStyle(elem).height, 10);
	};

    var getSpeed = function (distance, settings) {
		var speed = settings.speedAsDuration ? settings.speed : Math.abs(distance / 1000 * settings.speed);
		if (settings.durationMax && speed > settings.durationMax) return settings.durationMax;
		if (settings.durationMin && speed < settings.durationMin) return settings.durationMin;
		return parseInt(speed, 10);
	};

    var getDocumentHeight = function () {
		return Math.max(
			document.body.scrollHeight, document.documentElement.scrollHeight,
			document.body.offsetHeight, document.documentElement.offsetHeight,
			document.body.clientHeight, document.documentElement.clientHeight
		);
    };
    
    var easingPattern = function (settings, time) {
		var pattern;

		// Default Easing Patterns
		if (settings.easing === 'easeInQuad') pattern = time * time; // accelerating from zero velocity
		if (settings.easing === 'easeOutQuad') pattern = time * (2 - time); // decelerating to zero velocity
		if (settings.easing === 'easeInOutQuad') pattern = time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time; // acceleration until halfway, then deceleration
		if (settings.easing === 'easeInCubic') pattern = time * time * time; // accelerating from zero velocity
		if (settings.easing === 'easeOutCubic') pattern = (--time) * time * time + 1; // decelerating to zero velocity
		if (settings.easing === 'easeInOutCubic') pattern = time < 0.5 ? 4 * time * time * time : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1; // acceleration until halfway, then deceleration
		if (settings.easing === 'easeInQuart') pattern = time * time * time * time; // accelerating from zero velocity
		if (settings.easing === 'easeOutQuart') pattern = 1 - (--time) * time * time * time; // decelerating to zero velocity
		if (settings.easing === 'easeInOutQuart') pattern = time < 0.5 ? 8 * time * time * time * time : 1 - 8 * (--time) * time * time * time; // acceleration until halfway, then deceleration
		if (settings.easing === 'easeInQuint') pattern = time * time * time * time * time; // accelerating from zero velocity
		if (settings.easing === 'easeOutQuint') pattern = 1 + (--time) * time * time * time * time; // decelerating to zero velocity
		if (settings.easing === 'easeInOutQuint') pattern = time < 0.5 ? 16 * time * time * time * time * time : 1 + 16 * (--time) * time * time * time * time; // acceleration until halfway, then deceleration

		// Custom Easing Patterns
		if (!!settings.customEasing) pattern = settings.customEasing(time);

		return pattern || time; // no easing, no acceleration
	};
    
    return scope;
}
