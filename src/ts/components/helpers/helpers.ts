import * as THREE from 'three';
import { TYPE_TEXTURE, TYPE_IMAGE } from 'components/constants';

declare const BASE_URL;
declare const EXTERNAL_ASSETS;
declare const COLOR_FEEDER_DATA;
declare const PRODUCT_HANDLE;

export function getColorFeederData(){
    return COLOR_FEEDER_DATA;
}

export function capitalizeString(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function qs(query){
    return document.querySelectorAll(query);
}

export function isDefined(variable){
    return typeof variable !== 'undefined' &&  variable !== null;
}

export function getExternalAssetsUrl(){
    if(typeof EXTERNAL_ASSETS !== 'undefined'){
        return EXTERNAL_ASSETS;
    }
    return "";
}

export function getBaseUrl(){
    if(typeof BASE_URL !== 'undefined'){
        return BASE_URL;
    }
    return "";
}

export function detectModelType(url){
        
    let extension = url.slice((url.lastIndexOf(".") - 1 >>> 0) + 2);
    return extension.toLowerCase();
}

export function detectType(type: string){

    if(type == 'texture'){
        return TYPE_TEXTURE;
    }else if(type == 'normal'){
        return TYPE_TEXTURE;
    }else if(type == 'stitch'){
        return TYPE_TEXTURE;
    }else if(type.includes('_mask') || type.includes('_normal') || type.includes('_text')){
        return TYPE_TEXTURE;
    }
    return TYPE_IMAGE;
}

export function createWhiteTexture() {

    const canvas = document.createElement('canvas');
    canvas.width = 1024;
    canvas.height = 512;

    const ctx = canvas.getContext('2d');
    ctx.fillStyle = '#FFFFFF';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const texture = new THREE.CanvasTexture(canvas);
    texture.flipY = false;

    return texture;
}

export function getOriginalHandle(){
    if(typeof PRODUCT_HANDLE !== 'undefined' && PRODUCT_HANDLE !== null){
        return PRODUCT_HANDLE;
    }
    return undefined;
}

export function isLocalhost(){
    return window.location.hostname == "localhost" || window.location.hostname === "127.0.0.1";
}

export function fullFeederString(number){
    return 'Feeder '+number;
}

export function getColorsListIdName(name){
	let fixedName = String(name).trim().toLowerCase().replace(/ /g, "_"); // replace all spaces with _
    return 'colors_' + fixedName;
}

export function parseColor(colorCode, exportFormat: string = 'css'){

    let color;

    if(typeof colorCode === 'string' && colorCode.includes('#')){
        color = colorCode.replace('#','');
    }else if((typeof colorCode === 'number' || typeof colorCode === 'string') && String(colorCode).startsWith('0x')){
        color = String(colorCode).replace('0x','');
    }else if(typeof colorCode === 'object' && colorCode.hasOwnProperty('r')){
        color = rgbToHex(colorCode.r, colorCode.g, colorCode.b);
    }else{
        color = colorCode;
    }

    if(exportFormat == 'clean'){
        return color.toUpperCase();
    }else  if(exportFormat == 'threejs' || exportFormat == 'hex'){
        return '0x' + color.toUpperCase();
    }else{ // default - css
        return '#' + color;
    }
}

export function hexToRGB(hex){
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

export function rgbToHex(r: number, g: number, b: number){
    return '' + componentToHex(r) + componentToHex(g) + componentToHex(b);
};

export function componentToHex(c: number){
    const hex = c.toString(16).toUpperCase();

    return hex.length == 1 ? "0" + hex : hex;
};

export function getPointInBetweenByPerc(a, b, percentage) {

    var dir = b.clone().sub(a);
    var len = dir.length();
    dir = dir.normalize().multiplyScalar(len*percentage);
    return a.clone().add(dir);

}

export function getPointInBetweenByValue(a, b, distance) {

    var dir = b.clone().sub(a).normalize().multiplyScalar(distance);
    return a.clone().add(dir);

}

export function getConfigOrDefaultValue(object, defaultValue) {

    let value;
    if (isDefined(object)) {value = object;}
    else { value = defaultValue;}
    return value

}

export function opacityToHexAlpha(a) {
        
    let n = Math.floor(a*255).toString(16);
    let hex = + n + n + n;
    return hex;

}
