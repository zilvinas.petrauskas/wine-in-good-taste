
import * as THREE from 'three';
import { EVENT_CUSTOMIZER_SCENE_READY, EVENT_ADD_TO_SCENE } from 'components/constants';
import { Customizer } from 'Customizer';

export class SceneLights{

    constructor(){

        Customizer.app.action.listen(EVENT_CUSTOMIZER_SCENE_READY, this.init.bind(this));
    }

    protected init(){

        this.pointLight1();

        this.pointLight2();
        this.pointLight3();


        this.hemiLight();
        // this.areaLight1();
        // this.areaLight2();


    }

    protected hemiLight(){

        var hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.65);
        hemiLight.position.set(5, 0, 5);

        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, hemiLight);
    }

    protected ambientLight(){

        var ambientLight = new THREE.AmbientLight(0xffffff, 0.1);

        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, ambientLight);
    }

    protected pointLight1(){

        var light1 = new THREE.PointLight( 0xffffff, 0.2 );
        light1.position.set(0, 500, 0);
        light1.position.multiplyScalar( 1 );
        // light1.castShadow = true;

        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, light1);

    }

    protected pointLight2(){

        var light1 = new THREE.PointLight( 0xffffff, 0.3 );
        light1.position.set(50, 0, 500);
        light1.position.multiplyScalar( 1 );
        // light1.castShadow = true;

        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, light1);

    }

    protected pointLight3(){

        var light1 = new THREE.PointLight( 0xffffff, 0.3 );
        light1.position.set(125,75,-500);
        light1.position.multiplyScalar( 1 );
        // light1.castShadow = true;

        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, light1);

    }

    protected areaLight1() {
        
        var light1 = new THREE.RectAreaLight(0xc7d0aa, 0.5, 25, 500);
        light1.position.set(-500, 0, -200);
        light1.position.multiplyScalar( 1 );
        light1.lookAt(0,0,0);

        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, light1);

    }

    protected areaLight2() {
        
        var light1 = new THREE.RectAreaLight(0xc7d0aa, 0.5, 25, 500);
        light1.position.set(500, 0, -200);
        light1.position.multiplyScalar( 1 );
        light1.lookAt(0,0,0);

        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, light1);

    }

}
