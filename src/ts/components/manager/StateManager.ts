import { EVENT_STATE_READY, EVENT_SILENT_LOAD, EVENT_STATE_LOADING, EVENT_STATE_VISIBLE, EVENT_CUSTOMIZER_VISIBLE, EVENT_CUSTOMIZER_READY, EVENT_RESUME_CUSTOMIZER } from 'components/constants';
import { Customizer } from 'Customizer';


export class StateManager{
    
    private states : any;
    private currentState : number = 0;
    private done: boolean;
    private silent : boolean = false;
    private showCustomizer : boolean = false;

    constructor(){

        this.parseConfig();

        this.done = false;
        this.init();
        this.listen();

        if( ! this.silent ) this.scrollTop();
    }

    protected parseConfig(){

        let websiteConfig = Customizer.app.config.website.get();

        if(typeof websiteConfig !== 'undefined' && websiteConfig !== null){

            if(websiteConfig.hasOwnProperty('silentMode') && websiteConfig.silentMode === true){
                console.log('- Silent mode - ON');
                this.silent = true;
            }

           
        }

    }

    protected parseParams(params?){
        if(typeof params !== 'undefined' && params !== null ){
            for(let key in params){
                if(this.hasOwnProperty(key)){
                    this[key] = params[key];
                }
            }
        }
    }

    protected init(){

        this.states = {
            LOADING: 1,
            READY: 2,
            VISIBLE : 3,
            OUTRO: 9
        }
        
    }

    protected listen(){

        Customizer.app.action.register(EVENT_SILENT_LOAD, this.isSilentLoad.bind(this));
        Customizer.app.action.register(EVENT_STATE_LOADING, this.customizerLoading.bind(this));
        Customizer.app.action.register(EVENT_STATE_READY, this.customizerReady.bind(this));
        Customizer.app.action.register(EVENT_CUSTOMIZER_READY, this.customizerReady.bind(this));
        Customizer.app.action.register(EVENT_STATE_VISIBLE, this.customizerVisible.bind(this));
        Customizer.app.action.register(EVENT_RESUME_CUSTOMIZER, this.customizerResume.bind(this));

    }

    // triggered by progress bar if customizer is still loading
    public customizerLoading(){

        if(this.currentState < this.states.LOADING) this.currentState = this.states.LOADING

        if( ! this.silent ){
           this.addLoadingClassName();
        }
    }
    
    protected addLoadingClassName(){
        document.getElementsByTagName('html')[0].classList.add('customizer-loading');
    }

    // triggered by progress bar when loading is complete, but customizer might not be visible (if in silent mode)
    public customizerReady(){

        let delay = 1; // TODO - delay within config ?

        if(this.currentState === this.states.LOADING){
             delay = this.visibilityDelay(); // give time for threejs to create thigns TEMP solution for now
             this.currentState = this.states.READY;
        }

        if( this.showCustomizer || ! this.silent ){
            // .customizer-loaded

            this.currentState = this.states.VISIBLE;

            setTimeout(()=>{
                Customizer.app.action.call(EVENT_CUSTOMIZER_VISIBLE, null, null);
            }, delay);

          
        }
    }

    // show customizer 
    public customizerVisible(){
        
        this.scrollTop();
        this.showCustomizer = true;
        this.customizerReady();

    }

    public customizerResume(...args){

        let params = args[0] || {};

        this.scrollTop();

        if(this.currentState === this.states.LOADING){

            if(params.hasOwnProperty('silent')) this.silent = params.silent;
            else this.silent = false;

            this.addLoadingClassName();

        }else if(this.currentState === this.states.READY || this.currentState === this.states.VISIBLE){

            if(params.hasOwnProperty('silent')) this.silent = params.silent;
            this.customizerVisible();

        }

    }

    protected visibilityDelay(){
        return 1000;
    }

    protected scrollTop(){
        window.scrollTo(0,0);
    }


    protected isSilentLoad(){
        return this.silent;
    }

    public isReady(){
        return this.done;
    }
    
}