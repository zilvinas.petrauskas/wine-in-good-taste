import * as THREE from 'three';

import { Customizer } from 'Customizer';
import { isDefined, parseColor } from '../helpers/helpers'

import { EVENT_PAYLOAD_LIST , EVENT_UPDATE_CANVAS} from 'components/constants';

export class CustomCanvasTexture {

    private key;
    private productConfig;
    private canvasConfig;

    private labels = {} as any;
    private images = {} as any;

    private canvas;   // for color
    private ctx;
    private aCanvas;  // for alpha
    private aCtx;
    private rCanvas;  // for roughness
    private rCtx;

    private w = 1024;
    private h = 1024;
    
    constructor(...args) {

        this.key = args[0];
        this.productConfig = args[1];
        this.canvasConfig = args[2];
        
    }

    protected async initCanvases() {

        [this.canvas, this.ctx] = await this.createCanvas(this.key + "-canvas", this.w, this.h);
        [this.aCanvas, this.aCtx] = await this.createCanvas(this.key + "-alpha-canvas", this.w, this.h);
        [this.rCanvas, this.rCtx] = await this.createCanvas(this.key + "-roughness-canvas", this.w, this.h);

        return new Promise(resolve => resolve());
    }

    protected createCanvas(id, width, height) {

        let canvas, ctx;
        canvas = document.createElement("canvas");
        if (isDefined(id)) canvas.id = id;
        canvas.width = width;
        canvas.height = height;
        ctx = canvas.getContext('2d');

        return [canvas, ctx];

    }


    protected async fillCanvas(...args) {

        let ctx = args[0];
        let color = args[1];
        let composite = args[2]

        if (isDefined(composite)) {
            ctx.globalCompositeOperation = composite;
        }
        ctx.fillStyle = color;
        ctx.fillRect(0,0, this.w, this.h);

        return new Promise(resolve => resolve());

    }

    protected async init() {

        await this.initCanvases(); // didnt wait for this to finish and inited labels
        await this.initAllLabels();

        let opacity = parseColor(this.canvasConfig.opacity);
        let baseColor = parseColor(this.canvasConfig.baseColor);

        this.fillCanvas(this.ctx, baseColor, "destination-atop");
        this.fillCanvas(this.aCtx, opacity, "destination-atop");

    }

    // TODO - optimization - call get preloaded image once at the start of fn
    protected async initAllLabels() {

        console.log('-- initAllLabels');

        for (let i = 0 ; i < this.productConfig.layers.length; i++) {

            let layers = this.productConfig.layers;
            let layer = layers[i];
            if (layer.type == 'label' || layer.type == 'label-default') {

                // console.log("HEY", layer.type);

                if (layer.type == 'label') {
                    this.labels[layer.key] = await this.getPreloadedImage(layer.key);
                } else if (layer.type == 'label-default') {
                    this.labels[layer.key] = await this.createDefaultLabel(layer.config)
                } else {
                    console.log("Custom Canvas WARNING: Unknown layer type!")
                }

                if (isDefined(layer.default)) { 

                    this.images[layer.key] = await this.getPreloadedImage(layer.default);
                    let img =  this.images[layer.key];
                    await this.renderLabel(img, this.labels[layer.key])

                } else {

                    await this.renderLabel(null, this.labels[layer.key])

                }

            }

        }

        return new Promise(resolve => resolve());
    }

    protected async getPreloadedImage(key) {
        let preloaded = await Customizer.app.action.call(EVENT_PAYLOAD_LIST)
        if (!isDefined(preloaded)) {
            throw "WARN: key does not exist! Check product configuration"    
        }
        return preloaded[key];
    }

    protected async createDefaultLabel(config) {

        let c = config;

        let [tCanvas, tCtx] = this.createCanvas(null, this.w, this.h);
        
        tCtx.fillStyle = '#ffffff';
        tCtx.fillRect(c.offsetX, c.offsetY, c.width, c.height);

        return tCanvas;

    }

    protected async updateCanvas(...args) {

        let newImages = args[1]; // Label 

        // if (!this.verifyCanvasKeyInParts(parts)){
        //     console.warn('!! CANVAS PARTS NOT VERIFIED', parts);
        //     return new Promise(resolve => resolve());
        // }

        for (let i=0; i < newImages.length;i++) {
            let key = newImages[i].key;
            this.images[key] = newImages[i].image;
        }

        console.log('111 updateCanvas', newImages, this.images);

        await this.resetCanvases();

        for (var key in this.labels) {
            let img = this.images[key]
            await this.renderLabel(img, this.labels[key])
        }

        let opacity = parseColor(this.canvasConfig.opacity);
        let baseColor = parseColor(this.canvasConfig.baseColor);

        await this.fillCanvas(this.ctx, baseColor, "destination-atop");
        await this.fillCanvas(this.aCtx, opacity, "destination-atop");

    }

   
    protected async resetCanvases() {

        this.clearCanvas(this.canvas);
        this.clearCanvas(this.aCanvas);
        this.clearCanvas(this.rCanvas);

    }

    //REV: Needs better and more efficient methods than this
    protected clearCanvas(canvas) {
        canvas.width = canvas.width;
    }

    protected verifyCanvasKeyInParts(parts) {

        let verify = false;
        parts.forEach(part => {
            if (part.key === this.key) verify = true;
        });
        return verify;

    }

    protected async renderLabel(...args) {

        console.log('-- renderLabel', args);

        let img = args[0];
        let label = args[1];
        
        let lw = label.width;
        let lh = label.height;

        if (lw == 0 || lh == 0) {
            lw = this.w; lh = this.h;
        }

         // Temporary canvas and context for the current label
        let [tCanvas, tCtx] = await this.createCanvas(null, lw, lh);
        
        await tCtx.drawImage(label,0,0);
        tCtx.globalCompositeOperation="source-in";

        if (isDefined(img)) {

            // initial width and height
            let [initW, initH] = this.resizeImage(img, tCanvas, 'fill');
            let centerX = lw/2 - initW/2;
            let centerY = lh/2 - initH/2;
            tCtx.drawImage(img, centerX, centerY, initW, initH) // need position config

        } else {

            tCtx.fillStyle = "#ffffff"
            tCtx.fillRect(0,0, this.w, this.h);

        }

        // console.log('WHY?', typeof this.ctx, this.ctx);
        // console.log('tCanvas?', typeof tCanvas, tCanvas);
        this.ctx.drawImage(tCanvas, 0, 0);

        this.aCtx.globalCompositeOperation="source-over";
        this.aCtx.drawImage(tCanvas, 0, 0);
        this.aCtx.globalCompositeOperation="source-in";
        this.aCtx.fillStyle = "#FFFFFF"
        this.aCtx.fillRect(0,0, lw, lh);
        
        this.rCtx.globalCompositeOperation="source-over";
        this.rCtx.drawImage(tCanvas, 0, 0);
        this.rCtx.globalCompositeOperation="source-in";
        this.rCtx.fillStyle = "#00FF00" // roughness 1 on green channel
        this.rCtx.fillRect(0,0, lw, lh);

        return new Promise(resolve => resolve());

    }

    public async getCanvasTextures() {

        // REV: why do we need 2 canvases?
        // ANS: 1 for canvas, 1 for alpha, 1 for roughness.
        // All has to be different cos they're being used in different
        // maps
        let canvasTexture = new THREE.CanvasTexture(this.canvas);
        canvasTexture.flipY = false;
        let aCanvasTexture = new THREE.CanvasTexture(this.aCanvas)
        aCanvasTexture.flipY = false;
        let rCanvasTexture = new THREE.CanvasTexture(this.rCanvas)
        rCanvasTexture.flipY = false;

        canvasTexture.needsUpdate = true;
        aCanvasTexture.needsUpdate = true;
        rCanvasTexture.needsUpdate = true;

        return [canvasTexture, aCanvasTexture, rCanvasTexture];
    }


    protected resizeImage(img, target, method) {

        let w, h; // initial width and height

        if (method === 'fill') {
            if (img.width < img.height) {
                w = target.width;
                h = Math.round(w/img.width * img.height);
            } else {
                h = target.height;
                w = Math.round(h/img.height * img.width);
            }
        } else if (method === 'fit') {
            if (img.width < img.height) {
                h = target.height;
                w = Math.round(h/img.height * img.width);
            } else {
                w = target.width;
                h = Math.round(w/img.width * img.height);
            }
        }

        return [w,h];

    }

}
