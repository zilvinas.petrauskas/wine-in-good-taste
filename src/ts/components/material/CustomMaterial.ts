import * as THREE from 'three';
import { Customizer } from 'Customizer';
import { parseColor, getConfigOrDefaultValue, opacityToHexAlpha } from 'components/helpers/helpers';
import { CustomCanvasTexture } from './CustomCanvasTexture';

export class CustomMaterial {

    private debug = true;

    public material;
    private canvas;

    private baseColor;
    private opacity;
    
    private key;
    private options;
    private useCanvas;


    constructor(...args){

        if(this.debug) console.log('Custom material arguments', ...args);
        
		this.key = args[0];
        this.options = args[1];
		
    }

    // Initialize the base material 
    protected async initMaterial() {

        this.material = new THREE.MeshStandardMaterial();

        this.material.metalness = 0;
        this.material.depthWrite = false;
        this.material.depthTest = false;
        this.material.transparent = true;    

    }

    // FIX: this fix is a bit stupid but updateMaterial() implementatino doesnt leave me with many options. Z.
    protected async updateMaterialCanvas(labelImage){

        if(this.debug)  console.log('updateMaterialCanvas', this.canvas);

        await this.canvas.updateCanvas(
            [ 'partsArrayThatIsNotReallyUsed' ],
            [
                {
                    key: 'front-label',
                    image: labelImage
                }
            ]
        );

        let [c, a, r] =  await this.canvas.getCanvasTextures(); // CustomCanvasTexture

        this.material.map = c;
        this.material.alphaMap = a;
        this.material.roughnessMap = r;
        this.material.needsUpdate = true;

        return this.material;
    }

    protected async updateMaterial() {

        if (this.useCanvas) {

            // Defaulting to a blank canvas that doesn't affect CanvasTexture
            // adjustment for color and opacity will be done thru CanvasTexture
            this.material.opacity = 1;
            this.material.color = new THREE.Color(0xffffff);
            let [c, a, r] = await this.canvas.getCanvasTextures();
            this.material.map = c;
            this.material.alphaMap = a;
            this.material.roughnessMap = r;

        } else {

            this.material.opacity = this.opacity;
            this.material.color = new THREE.Color(parseColor(this.baseColor))

        }

        this.material.needsUpdate = true;

        return this.material;

    }

    public async init() {

        await this.checkKey();
        await this.initMaterial();
        let material = await this.updateMaterial();
        return material;

    }
    
    private async checkKey() {

        let productConfig = await Customizer.app.config.product.get();
        let modelConfig = productConfig.model;
        let canvasConfig = {} as any; // params to be passed on to customCanvasTexture

        this.baseColor = getConfigOrDefaultValue(modelConfig.baseColor, "ffffff")
        this.opacity = getConfigOrDefaultValue(modelConfig.opacity, 0.4)

        canvasConfig.baseColor = this.baseColor;
        canvasConfig.opacity = opacityToHexAlpha(this.opacity);

    // Check if there is any texture in part layers
        for (let j=0;j<productConfig.parts.length;j++) {

            let part = productConfig.parts[j];
            if (part.key == this.key) {

                let layers = part.layers

                if (this.checkForTypeInArrays(layers, 'label') || this.checkForTypeInArrays(layers, 'label-default')) {
                    
                    // setTimeout(async ()=>{
                        this.canvas = new CustomCanvasTexture(this.key, part, canvasConfig);
                        await this.canvas.init();
                        this.useCanvas = true;
                    // }, 1000)
                    break;
                    
                }

            }

        }

    }

    // REV: this will fail if first element in array is bad but 2nd is good. or the other way around
    protected checkForTypeInArrays(arrays, type) {

        let check = false;
        arrays.forEach(e => {
            if (e.hasOwnProperty('type')) {
                if (e.type === type) {
                    check = true;
                }
            }
        });
        return check;
    
    }

}
