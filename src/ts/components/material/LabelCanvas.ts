import { Customizer } from 'Customizer';
import { EVENT_UPDATE_2D_CANVAS_IMAGE, EVENT_ADJUST_CANVAS, EVENT_LABEL_IMAGE_SCALING, EVENT_GET_LABEL_CANVAS, EVENT_GET_LABEL_IMAGE, EVENT_RESET_SCALE, EVENT_PAYLOAD_LIST, EVENT_GET_PART_TO_CUSTOMIZE, EVENT_CREATE_MASK_CANVAS, EVENT_DESTROY_MASK_CANVAS } from 'components/constants';
import { isDefined } from '../helpers/helpers'
import { Layers } from 'three';
import { Body } from 'components/product/wine01/parts/Body';


export class LabelCanvas{

    protected canvas : any = {
        width: 1024, // desired width
        height: 1024, // and height
        ctx: undefined as any,
        element: undefined as any,
        id: '#label-canvas',
        dragging : false,
        offset : {
            x : 0,
            y : 0,
        } as any,

    }

    protected image: any = {
        element: undefined as any,
        x : 0,
        y : 0,
        width: 0,
        height: 0,
        scale: 1,
        prevScale: 1,
    }

    protected mouse: any = {
       start :{
        x : 0,
        y : 0,
       },
       last: {
        x : 0,
        y : 0,
       }
    }

    constructor(){

        this.prepare();
        this.listen();
    }

    protected listen(){
        Customizer.app.action.register(EVENT_UPDATE_2D_CANVAS_IMAGE, this.updateImage.bind(this));
        Customizer.app.action.register(EVENT_LABEL_IMAGE_SCALING, this.scaleImage.bind(this));

        // if you need IMAGE
        Customizer.app.action.register(EVENT_GET_LABEL_IMAGE, this.getLabelImage.bind(this));

        // if you need CANVAS
        Customizer.app.action.register(EVENT_GET_LABEL_CANVAS, this.getLabelCanvas.bind(this));

        Customizer.app.action.register(EVENT_CREATE_MASK_CANVAS,  this.createMaskCanvas.bind(this))
        Customizer.app.action.register(EVENT_DESTROY_MASK_CANVAS,  this.destroyMaskCanvas.bind(this))


    }

    protected bindEvents(){
        

        // desktop
        this.canvas.element.addEventListener('mousedown', this.onDragStart.bind(this));
        this.canvas.element.addEventListener('mouseup', this.onDragEnd.bind(this));
        window.addEventListener('mouseout', this.onDragEnd.bind(this));
        window.addEventListener('mousemove', this.onMovement.bind(this));

        // mobile
        this.canvas.element.addEventListener('touchstart', this.onDragStart.bind(this));
        this.canvas.element.addEventListener('touchend', this.onDragEnd.bind(this));
        this.canvas.element.addEventListener('touchmove', this.onMovement.bind(this));

    }

    protected prepare(){

        this.canvas.element = document.querySelectorAll( this.canvas.id )[0];
        if( this.canvas.element == undefined ){
            console.warn('!! Canvas element not found in html with id: ', this.canvas.id);
            return;
        }
        this.canvas.element.width = this.canvas.width;
        this.canvas.element.height = this.canvas.height;

        this.canvas.ctx = this.canvas.element.getContext('2d');

        this.onResize();
        this.bindEvents();
    }

    protected onResize(){

       

    }


   
    

    protected onDragStart(e: any){

        this.updateMousePosition(e, this.mouse.start);
        this.canvas.element.dragging = true;  
        this.updateView();
    }

    protected onDragEnd(e: any){

        this.updateMousePosition(e, this.mouse.start);
       
        this.canvas.element.dragging = false;
        this.updateView();
    }

    protected updateView(){

        const className = 'dragging';

        if(this.canvas.element.dragging && !document.body.classList.contains(className)){
            document.body.classList.add(className);
        }else if(!this.canvas.element.dragging && document.body.classList.contains(className)){
            document.body.classList.remove(className);
        }

    }

    protected async onMovement(e){

        if( ! this.canvas.element.dragging) return;

        this.updateMousePosition(e, this.mouse.last);

        // get difference
        var dx = this.mouse.last.x - this.mouse.start.x;
        var dy = this.mouse.last.y - this.mouse.start.y;

        // change image position
        this.image.x += dx;
        this.image.y += dy;
        
        this.mouse.start.x = this.mouse.last.x;
        this.mouse.start.y = this.mouse.last.y;

        await this.drawImage();
        

    }

    protected getLabelCanvas(){
        return this.canvas.element;
    }

    protected getLabelImage(){
        
        const image = new Image(this.canvas.element.width, this.canvas.element.height);
        image.src = this.canvas.element.toDataURL('image/png');
        return image;

    }

    /*
    NEW: scale from center position
    */
   // qq
    protected async drawImage(){

        const w = this.image.width * this.image.scale;
        const h = this.image.height * this.image.scale;

        let x = (this.image.width - w) / 2,
            y = (this.image.width - h) / 2; 
          
            // BUG - when image is initially create it doesnt account for scaling from center
            // qq3
            // console.log("CENTER #2", this.image.x, this.image.y);
        this.canvas.ctx.clearRect(0,0, this.canvas.element.width, this.canvas.element.height);
        this.canvas.ctx.drawImage(this.image.element, this.image.x + x, this.image.y + y, w, h);
        // this.canvas.ctx.drawImage(this.image.element, this.image.x, this.image.y, w, h);

    }

    /*
        e: mouse movement event
        mouse : either this.mouse.start OR this.mouse.drag
    */
    protected updateMousePosition(e, mouse){

        let eX = e.clientX || e.touches[0].pageX;
        let eY = e.clientY || e.touches[0].pageY;

        let offsetX = this.canvas.element.offsetLeft;
        let offsetY = this.canvas.element.offsetTop;

        mouse.x = eX - offsetX;
        mouse.y = eY - offsetY;
        
        // console.log(mouse.x, mouse.y)

        return true;
    }

    protected scaleImage(scaleFloat){

        if(this.image.element == undefined) return; // image not uploaded

        const scale = parseFloat(scaleFloat);

        if(scale > 0 && scale < 100){

            this.image.prevScale = this.image.scale;
            this.image.scale = scale;
            this.drawImage();

        }else{
            console.warn('!! Wrong label image scale value!', scaleFloat, scale);
        }

    }

    // update 2d canvas with new uploaded image
    protected async updateImage(...args){

        let image = args[0];
        const updateMaskCanvas = args[1] || false;

        image = await this.resizeImageToFitCanvas(image);

        await this.canvas.ctx.clearRect(0,0, this.canvas.element.width, this.canvas.element.height);
        
        // qq4
        const centerX = (this.canvas.element.width - image.width) / 2;
        const centerY = (this.canvas.element.height - image.height) / 2;
        await this.canvas.ctx.drawImage(image, centerX, centerY, image.width, image.height);

        Customizer.app.action.call(EVENT_RESET_SCALE); // reset html scaler
        this.saveImage(image); // start from 0,0 

       

    }

    protected saveImage(image, x=0, y=0, scale=1){

        this.image = {
            element: image,
            width: image.width,
            height: image.height,
            x : x,
            y : y,
            scale: scale,
            prevScale: scale,
        }
    }

    protected async resizeImageToFitCanvas(image){
        
        if(image.width > this.canvas.element.width || image.height > this.canvas.element.height ){

            const hr = this.canvas.element.width / image.width; // height ratio
            const vr = this.canvas.element.height / image.height; // width ratio
            const ratio  = Math.min ( hr, vr );

            const imageWidth = Math.round(image.width*ratio);
            const imageHeight = Math.round(image.height*ratio);

            // const tempCanvas = document.createElement('canvas');
            // tempCanvas.width = imageWidth;
            // tempCanvas.height = imageHeight;

            // // resize image
            // tempCanvas.getContext('2d').drawImage(image, 0,0, image.width, image.height, 0,0, imageWidth, imageHeight);

            // // console.log('image size', image.width, image.height);
            // // console.log('new image size', image.width * ratio, image.height * ratio);

            // REV : tempCanvas.toDataURL was the one causing the problem
            // not sure why, I imagine it has got to do with the transfer
            // of octet stream from one source to the other

            // const resizedImage = new Image(imageWidth, imageHeight);
            // resizedImage.src = tempCanvas.toDataURL('image/png');

            // return resizedImage;

            image.width = imageWidth;
            image.height = imageHeight;
            return image;

        }
        
        return image;
    }

    protected async createMaskCanvas() {

        let key = await Customizer.app.action.call(EVENT_GET_PART_TO_CUSTOMIZE);

        var parent = this.canvas.element.parentElement;
        var w = this.canvas.width;
        var h = this.canvas.height;

        var mCanvas = document.createElement("canvas");
        mCanvas.width = w;
        mCanvas.height = h;
        mCanvas.style.position = "absolute";
        mCanvas.style.pointerEvents = 'none';
        mCanvas.id = 'label-mask-canvas'
        var mCtx = mCanvas.getContext('2d');

        var labels = {};

    // Currently dependent on config, but need to make more dynamic
    // In case labels can change
    // At that point will definitely need a new class that stores label informations

        let productConfig = await Customizer.app.config.product.get();       

        for (let i = 0 ; i < productConfig.parts.length; i++) {


            if (productConfig.parts[i].key == key) {

                let layers = productConfig.parts[i].layers;

                for (let j = 0 ; j < layers.length; j++) {

                    let layer = layers[j];

                    if (layer.type === 'label') {
    
                        console.log('======================= label', layer.key);
                        labels[layer.key] = await this.getPreloadedImage(layer.key);
                    
                    } else if (layer.type === 'label-default') {

                        console.log('======================= label-default', layer.key);
                        labels[layer.key] = await this.createDefaultLabel(layer.config);

                    }
    
                }

            } 

        } 

        // draw labels on the same canvas
        
        for (var key2 in labels) {

            await mCtx.drawImage(labels[key2], 0, 0);

        }

        // convert them to low opacity
        mCtx.globalCompositeOperation = "source-out"
        mCtx.fillStyle = "rgba(255, 255, 255, 0.8)"
        await mCtx.fillRect(0,0, w,h);

        parent.appendChild(mCanvas)

    }

    protected async destroyMaskCanvas() {

        let mCanvas = document.getElementById('label-mask-canvas');
        
        if(typeof mCanvas !== 'undefined' && mCanvas !== null){
            var parent = this.canvas.element.parentElement;
            if(typeof parent !== 'undefined' && parent !== null){
                parent.removeChild(mCanvas)
                return true;
            }
        }

        return false;
    }

    protected async getPreloadedImage(key) {
        let preloaded = await Customizer.app.action.call(EVENT_PAYLOAD_LIST)
        if (!isDefined(preloaded)) {
            throw "WARN: key does not exist! Check product configuration"    
        }    // REV: double check if key exists, if not throw error in console
        return preloaded[key];
    }

    protected async createDefaultLabel(config) {

        let [tCanvas, tCtx] = await this.createCanvas(null, this.canvas.width, this.canvas.height);
        
        tCtx.fillStyle = '#ffff00';
        tCtx.fillRect(config.offsetX, config.offsetY, config.width, config.height);

        var img = new Image()
        img.src = await tCanvas.toDataURL("image/png")

        return img;

    }

    protected async createCanvas(id, width, height) {

        let canvas, ctx;
        canvas = document.createElement("canvas");
        if (isDefined(id)) canvas.id = id;
        canvas.width = width;
        canvas.height = height;
        ctx = canvas.getContext('2d');

        return [canvas, ctx];
    }

}