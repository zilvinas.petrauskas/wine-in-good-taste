import { EVENT_BUILD_MATERIAL, EVENT_UPDATE_MATERIAL, EVENT_GET_MODEL_PARTS, EVENT_GET_MODEL } from 'components/constants';
import { Customizer } from 'Customizer';
import { CustomMaterial } from './CustomMaterial';

export class MaterialFacade{
    
    private node = {} as any;

    constructor(...args){

        this.init();
    }    

    protected init(){

        Customizer.app.action.register(EVENT_BUILD_MATERIAL, this.material.bind(this), null);
        Customizer.app.action.register(EVENT_UPDATE_MATERIAL, this.updateMaterial.bind(this), null);

    }

     protected getCustomMaterialClass(...args){

        return new CustomMaterial(...args);
    }

    /*
        arguments
        [0] - key  (REV: is this a key from config?) ANS : yes
        [1] - options
        [2] - textParams[] (initial)
    */
    protected async material(...args){

        let key = args[0];

        this.node[key] = this.getCustomMaterialClass(...args);
        let material = await this.node[key].init();

        material.needsUpdate = true;

        return new Promise(resolve => resolve(material));      
    }

   

    protected async updateMaterial(...args){

        const key = args[0];
        const labelImage = args[1];

        console.log(...args);

        // CustomMaterial.ts -> updateMaterialCanvas
        await this.node[key].updateMaterialCanvas(labelImage); 
        
    }


}