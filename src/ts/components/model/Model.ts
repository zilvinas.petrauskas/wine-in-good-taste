import * as THREE from 'three';
import { Customizer } from 'Customizer';
import { EVENT_ASSIGN_PARTS, EVENT_GET_COMPONENT_BY_KEY, EVENT_ADD_TO_SCENE, EVENT_MODEL_READY, EVENT_UPDATE_PARTS_MATERIALS, EVENT_UPDATE_LABEL_MATERIAL, EVENT_PROGRESS_ADD, TYPE_GLB, EVENT_PAYLOAD_FINISH, EVENT_GET_MODEL, EVENT_GET_MODEL_DIMENSIONS, EVENT_GET_MODEL_PARTS, EVENT_GET_PRODUCT_POSITION, EVENT_GET_MODEL_NAME, EVENT_TEXTURES_APPLIED, EVENT_BUILD_MATERIAL, EVENT_UPDATE_MATERIAL } from 'components/constants';
import { MaterialFacade } from 'components/material/MaterialFacade';
import { isLocalhost } from 'components/helpers/helpers';


export class Model{ // default sweater for Calamigos - front, back, sleeve (2x but uses same texture) with textures and then collar, rib & cuff with just color change

    public modelConfig = {
        model: {},
        parts : []
    } as any;

    protected model: any;
    protected preloaded: any;
    
    protected async init(...args){

        this.preloaded = args[0]; // loadTextures
        await this.create();
       
    }

    protected async create(){
        
        this.model = this.preloaded[ this.modelConfig.model.name ];
        this.model = await this.applyOptions(this.model, this.modelConfig.options);
 
        await this.afterModelCreation();

        await Customizer.app.action.call(EVENT_ASSIGN_PARTS, null, null, this.model);
        
        await this.applyInitialMaterials();
        await Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, this.model);

        Customizer.app.action.broadcast(EVENT_MODEL_READY, null);
        Customizer.app.action.call(EVENT_PROGRESS_ADD, null, null, 'product');

        
    }

    protected async afterModelCreation(){
        
    }

    public getModelName(){
        return this.modelConfig.model.name;
    }

    // add 3d model to preloader
    protected queueModel(){
        var loadExternalAsset = isLocalhost();
        Customizer.app.payload.add(this.modelConfig.model.name, this.modelConfig.model.file, TYPE_GLB, loadExternalAsset);
    }

    protected listen(){

        Customizer.app.action.listen(EVENT_PAYLOAD_FINISH, this.init.bind(this) );

        Customizer.app.action.register(EVENT_GET_MODEL, this.getModel.bind(this));
        Customizer.app.action.register(EVENT_GET_MODEL_DIMENSIONS, this.getModelDimensions.bind(this));
        Customizer.app.action.register(EVENT_GET_MODEL_PARTS, this.getModelParts.bind(this), null);
        Customizer.app.action.register(EVENT_GET_PRODUCT_POSITION, this.getProductPosition.bind(this), null);
        Customizer.app.action.register(EVENT_GET_MODEL_NAME, this.getModelName.bind(this));
        Customizer.app.action.register(EVENT_GET_COMPONENT_BY_KEY, this.getComponentByKey.bind(this));

        Customizer.app.action.register(EVENT_UPDATE_LABEL_MATERIAL, this.updatePartsMaterials.bind(this));

    }


    protected getModel(){
        return this.model;
    }

    protected getModelParts(){
        return this.modelConfig.parts;
    }

    public getModelDimensions() {

        const box = new THREE.Box3().setFromObject( this.getModel() );
        const center = box.getCenter(new THREE.Vector3());
        const size = box.getSize(new THREE.Vector3());

        return {
            size: size, 
            center: center,
        };
    }

    protected async getProductPosition(){
        
        const dimensions = this.getModelDimensions();
        return new Promise(resolve => resolve(dimensions.center));

    }
   
    protected async applyOptions(model, options){

        if(typeof options !== 'undefined' && options !== null){
            if(options.scale) model.scale.set(options.scale, options.scale, options.scale);
            if(options.position) model.position.copy(options.position);
            if(options.rotation) model.rotation.copy(options.rotation);
        }

        return new Promise(resolve => {
            resolve(model);
        })
    }

    protected async applyInitialMaterials(){


        for(let i=0;i< this.modelConfig.parts.length; i++){              
               await this.applyMaterial(this.modelConfig.parts[i].component);
        }

       Customizer.app.action.broadcast(EVENT_TEXTURES_APPLIED, null);

    }

    protected async applyMaterial(component, material:any = undefined){

        const componentEventName = component.getEventName();
        const componentKey = component.getKey();

        // get material THRU EVENT
        if(material == undefined)
            material = await Customizer.app.action.call(EVENT_BUILD_MATERIAL, null, true, componentKey, this.preloaded) as THREE.Material;

        if(typeof material === 'undefined' || material == undefined || material === null){
            console.warn('!! Material for change not found', componentKey, material);
            return new Promise(resolve => resolve());
        }

        // get parts as 3D objects that will be modified
        await Customizer.app.action.call(componentEventName, {passTriggerCallback: true}, (mesh)=>{
            if(typeof mesh === 'undefined' || mesh === null){
                console.warn('Mesh not found in triggerMaterialChange', componentKey);
                return new Promise(resolve => resolve());
            }

            if (mesh.material) mesh.material.dispose();

            material.needsUpdate = true;
            mesh.material = material;
            mesh.material.needsUpdate = true;
            
            return new Promise(resolve => resolve());

        });

    }

    protected async updatePartsMaterials(...args){

        const parts = args[0];
        const labelImage = args[1]; // labelImage

        for(let i=0;i<parts.length;i++){
            // goes to MaterialFacade.ts updateMaterial()
            await Customizer.app.action.call(EVENT_UPDATE_MATERIAL, null, null,  parts[i].key, labelImage);
        }

    }

    private getComponentByKey(key){

        for(let i=0;i< this.modelConfig.parts.length; i++){
            if(this.modelConfig.parts[i].key == key){
                return this.modelConfig.parts[i].component;
            }
        }
        
        return undefined;
    }

 

}