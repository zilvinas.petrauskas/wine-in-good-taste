import { Object3D } from 'three';
import { EVENT_ASSIGN_PARTS } from 'components/constants';
import { Customizer } from 'Customizer';

export class Part{

    protected parts = ['part1name','part2name'];
    protected partEventName = 'component-event-name';
    protected key = 'component-key';
    protected objects: any;
    

    constructor(...args){

    }

    protected listen(){

        Customizer.app.action.register(EVENT_ASSIGN_PARTS, this.find.bind(this), { unique: false });
        Customizer.app.action.register(this.partEventName, this.update.bind(this));
               
    };

    /*
        call this thru event to receive mesh of parts assigned to specific group
        
        RETURNS 3D MESH TO A CALLBACK
    */
    protected update(callback){

        // callback requires an model part (object 3d or Mesh) to be returned
        // so go thru all objects and sent dthem back to callback

        for(let name in this.objects){
            if(this.objects.hasOwnProperty(name)){
                callback(this.objects[name]);
            }
        }
        
    }

    public getEventName(){
        return this.partEventName;
    }

    public getKey(){
        return this.key;
    }

    // find this.parts in model3D object
    // only on initialization of things, later on these are held within Parts class in this.objects variable
    protected async find(...args){

        let model = args[0];
        let scope = this;
        this.objects = {};
        let found = 0;
        let done = false;

        let findWithin = async (_model) => {

            if(scope.parts.includes(_model.name)){
                assignPart(_model);
            }
            
            if(_model.children.length > 0){
                _model.children.forEach((child)=>{
                    if(scope.parts.includes(child.name)){
                        assignPart(child);
                    }
                    if(!done) findWithin(child);
                });
            }

        }

        let assignPart = (_model)=>{
            
            for(let i=0;i<scope.parts.length;i++){
                if(scope.parts[i] == _model.name){
                    scope.objects[_model.name] = _model;
                    found++;
                    if(scope.parts.length == found){
                        done = true;
                    }
                }
            }
        }


        await findWithin(model);

        if(Object.keys(this.objects).length < this.parts.length){
            console.warn('-- NOT ALL PARTS WERE FOUND', this.parts, this.objects);
        }

        return this.objects;
    }

}
