import { Customizer } from 'Customizer';
import { detectType, isLocalhost } from 'components/helpers/helpers';
import { WineBottle } from './wine01/WineBottle';
import { TextureLoader, BoxBufferGeometry, MeshBasicMaterial, Mesh, Color, Clock } from 'three';
import { EVENT_ADD_TO_SCENE, EVENT_ADD_TO_RENDERER } from 'components/constants';

export class Product{

    private productConfig : any;

    constructor(){
        
         /*
            1. get product config
            2. preload textures (based of config)
            3. preload product module
        */
      
        this.prepare();
    }

   

    protected async prepare(){

        this.productConfig = Customizer.app.config.product.get()

        console.log('got it?', this.productConfig);

        await this.textures()
        await this.model()

    }


    protected async model(){

        let criticalError = this.detectModelErrors();
        if(criticalError) return;

        let model = undefined;

        switch(this.productConfig.model.name){
            case 'wine-bottle':   model =  new WineBottle();    break;
            default:
            break;
        }

        if(model == undefined){
            console.warn('Model by name '+this.productConfig.model.name+' NOT FOUND in ModelPicker');
        }else{
            
        }
    }

    


    protected async textures(){

        // types not defined in here, will not be loaded from product's layers
       

        if(this.productConfig.hasOwnProperty('parts')){
            this.prepareParts();
        }

        if(this.productConfig.hasOwnProperty('labels')){
            // TODO prepare labels
        }

    }

    protected prepareParts(){

        let preloadTypes = [ 
            'label',
            'image', 
            'normal',
            'stitch',
            'stitch_normal',
            'stitch_mask',
            'stitch_text',
        ];

        var loadExternalAsset = isLocalhost(); // true only on loaclhost for now

        for(let i=0;i<this.productConfig.parts.length;i++){
            
            let part = this.productConfig.parts[i];

            for(let y=0;y<part.layers.length;y++){

                let layer = part.layers[y];

                if(preloadTypes.includes(layer.type)){
                    Customizer.app.payload.add(layer.key, layer.value, detectType(layer.type), loadExternalAsset); // TODO - load externally on live
                    // Root.instance.preloader.add('unique-textre-key', 'textures-of-front.ng', )
                } else if (layer.type == 'label' || layer.type =='texture') {
                    Customizer.app.payload.add(layer.key, layer.value, detectType(layer.type), loadExternalAsset);   // TODO - load externally on live
                } else if(layer.type == 'color' || layer.type == undefined){
                    // doesnt need to be preloaded, but also shouldnt throw a warning
                }else if(layer.type == 'text'){
                    // fine also, nothing to preload
                }else if(layer.type == 'label-default'){
                    // fine also, nothing to preload
                }else{
                    console.warn('Unknown layer type not allowed! --->', layer.type);
                }

            }
        }

    }


    protected detectModelErrors(){

        if( typeof this.productConfig === 'undefined' || this.productConfig === null){
            console.warn('!! Product config not found!', this.productConfig);
        }

        if( ! this.productConfig.hasOwnProperty('model')){
            console.warn('Product config doesnt contain model information - "model" ', this.productConfig);
            return true;
        }

        if( ! this.productConfig.hasOwnProperty('model')) console.warn('Define parameter "model" within product configuration');
        if( ! this.productConfig.model.hasOwnProperty('name')) console.warn('Define parameter "name" for model object within product configuration');

        return false;
    }

    

}