import { Model } from 'components/model/Model';
import { Vector3, PlaneGeometry, MeshBasicMaterial, DoubleSide, Mesh, Math } from 'three';
import { Front } from './parts/Front';
import { Back } from './parts/Back';
import { Body } from './parts/Body';
import { Customizer } from 'Customizer';
import { EVENT_ADD_TO_SCENE } from 'components/constants';

export class WineBottle extends Model{
    
    constructor(){
        super();

        this.modelConfig.model = {
            name: 'wine-bottle',
            file: 'wine-bottle-v7.glb',
        }

        // label will be created dynamically?
        this.modelConfig.parts = [
            { 
                key: 'front',
                component: new Front(),
            },
            { 
                key: 'back',
                component: new Back(),
            },
            { 
                key: 'body',
                component: new Body(),
            }
        ]

        this.modelConfig.options = {
            scale: 100,
            // position: new Vector3(0,20,0),
        }

        this.queueModel(); // push 3d model url to preloader
        this.listen(); // wait for preloader to finish and call 'init' function


        // this.createPlaneCenteredVertically();        
    }

    protected async afterModelCreation(){

        // DO CUSTOMIZATIONS TO WINE BOTTLE IF NEEDED

    }


    protected createPlaneCenteredVertically(){

        var geometry = new PlaneGeometry( 100, 100, 32 );
        var material = new MeshBasicMaterial( {color: 0xffff00, side: DoubleSide} );
        var plane = new Mesh( geometry, material );
        plane.rotateX( Math.degToRad(90) )
        
        Customizer.app.action.call( EVENT_ADD_TO_SCENE, null, null, plane );
    }

}