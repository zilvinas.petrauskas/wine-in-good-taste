import { Part } from 'components/model/Part';

export class Body extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['body'];
        this.partEventName = 'update-body';
        this.key = 'body';
        this.listen();
    }
}