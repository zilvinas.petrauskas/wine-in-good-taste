import { Part } from 'components/model/Part';

export class Cylinder extends Part{
    constructor(...args){
        super(...args);

        this.parts = ['cylinder'];
        this.partEventName = 'update-cylinder';
        this.key = 'cylinder';
        this.listen();
    }
}