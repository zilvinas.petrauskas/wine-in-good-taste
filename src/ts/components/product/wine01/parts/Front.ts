import { Part } from 'components/model/Part';

export class Front extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['FRT'];
        this.partEventName = 'update-front';
        this.key = 'front';
        this.listen();
    }
}