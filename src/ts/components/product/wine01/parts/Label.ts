import { Part } from 'components/model/Part';

export class Label extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['label'];
        this.partEventName = 'update-label';
        this.key = 'label';
        this.listen();
    }
}