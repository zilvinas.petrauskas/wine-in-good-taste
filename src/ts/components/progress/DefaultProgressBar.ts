import { EVENT_ADJUST_PROGRESS_CONFIG, EVENT_PROGRESS_BAR_STARTED, EVENT_PROGRESS_ADD } from 'components/constants';
import { Customizer } from 'Customizer';

export class DefaultProgressBar {

    protected config: any = {
        initial: 100,
    }

    protected increase: any = {
        initial: 1,
    }

    protected currentProgress: number = 0; // progress shown in visual loader
    protected waitingProgress: number = 0; // progress thats done but waiting because of animation delay (smoothness)

    protected updateTexts: boolean = false; // if texts in progress should be updated

    protected texts = {
        // 0 : 'Loading...',
        // 50: 'Half way thru!',
        // 75: 'Almost done..',
        // 90: 'Get ready!',
    }

    private textContainer: HTMLElement; // where text value should changed, modified
    private progressValueContainer: HTMLElement; // where progress number should be changed
    private progressContainer: HTMLElement; // wrapper/container of whole progress container - for showing/hiding

    private completed: boolean = false;
    private loop: any;

    constructor(_config?) {

        if (typeof _config !== 'undefined') {
            if (_config.textContainer) this.textContainer = _config.textContainer;
            if (_config.progressValueContainer) this.progressValueContainer = _config.progressValueContainer;
            if (_config.progressContainer) this.progressContainer = _config.progressContainer;
        }

        this.listen();
    }

    protected listen() {

        // TODO - move these events to progressBar.ts - they arent global and each extended version should have their own events

        this.adjustProgressConfig();
        this.progressAdd();
        this.progressBarStart();

    }

    protected adjustProgressConfig() {
        Customizer.app.action.register(EVENT_ADJUST_PROGRESS_CONFIG, this.adjust.bind(this));
    }

    protected progressAdd() {
        Customizer.app.action.register(EVENT_PROGRESS_ADD, this.add.bind(this));
    }

    protected progressBarStart() {
        Customizer.app.action.register(EVENT_PROGRESS_BAR_STARTED, this.start.bind(this));
    }

    protected reset(){
        this.currentProgress = 0;
        this.waitingProgress = 0;
        this.completed = false;
    }

    /*
        type of config param
        total - amount of items to be loaded

        - for 'prelaoder' type we need to know how many items will be prelaoder and calc one item progress increase
        - leftover gets added to initial
    */
    protected adjust(type, total) {

        if (!this.config.hasOwnProperty(type)) {
            console.warn('Unknown type for progress bar config update', type);
            return;
        }

        const progress = this.config[type];
        const increase = Math.floor(progress / total);
        const leftover = progress - increase;

        if (leftover > 0) this.config.initial += leftover;
        this.increase[type] = increase;

    }

    protected add(type) {

        if (!this.increase.hasOwnProperty(type)) {
            console.warn('Unknown type for progress bar increase', type, this.increase[type], this.increase);
            return;
        }

        this.waitingProgress += this.increase[type];

    }

    /*
        show progress bar
        start animating initial load
    */
    protected start() {

        this.waitingProgress += this.config.initial; // set initial progress loading value - so that users would feel like things are loading instantly
        this.updateTextsWhileLoading();
        this.show();
        this.animate(true);  // to stop - clearTimeout(this.loop);


    }

    protected show() {

        document.getElementsByTagName('html')[0].classList.add('customizer-loading');

    }

    protected hide() {

        document.getElementsByTagName('html')[0].classList.remove('customizer-loading');

    }


    protected animate = (loop: boolean = false) => {

        if (!this.completed) {

            if (this.currentProgress >= 100) {

                this.completed = true;
                this.onComplete();

            } else if (this.currentProgress < this.waitingProgress) {

                // this.currentProgress += 1;
                this.currentProgress = this.waitingProgress;
                this.currentProgress = Math.min(100, this.currentProgress); // cap at 100
                this.updateVisibleProgressValue();
                this.onUpdate();

            }

            if (loop === true) {
                let normal = 1000 / 50;
                let faster = 1000 / 90; // shorter delay when visual progress is way behind actual progress
                let delay = this.waitingProgress - this.currentProgress > 33 ? faster : normal;

                // this.loop = setTimeout(() => {
                //     this.animate(true);
                // }, delay);

                this.loop = requestAnimationFrame(()=>{
                    this.animate(true);
                })

            }

        }



    }

    protected updateVisibleProgressValue() {

        // console.log('progress', this.currentProgress);

        if (this.progressValueContainer) this.progressValueContainer.style.width = this.currentProgress + '%';
    }

    protected onUpdate() {
        // in case you wanna do stuff when progress changes

        this.updateTextsWhileLoading();

    }

    protected updateTextsWhileLoading() {
        if (this.updateTexts && this.texts.hasOwnProperty(this.currentProgress)) {
            this.textContainer.innerHTML = this.texts[this.currentProgress];
        }
    }

    protected onComplete() {
        alert('Missing on complete function!');
    }


}