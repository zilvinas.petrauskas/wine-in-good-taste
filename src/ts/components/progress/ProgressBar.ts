import { EVENT_CUSTOMIZER_READY, EVENT_CUSTOMIZER_LOADING, EVENT_STATE_LOADING, EVENT_STATE_VISIBLE, EVENT_CUSTOMIZER_VISIBLE } from 'components/constants';
import { DefaultProgressBar } from './DefaultProgressBar';
import { Customizer } from 'Customizer';

export class ProgressBar extends DefaultProgressBar{

    protected config: any = {
        initial: 50, // just load to percentage without waiting for code
        preloader: 40, // shared between all preloader items
        product: 5, // after creation of product
        ui: 5, // after creation of ui
    }

    protected increase: any = {
        initial: 1,
        preloader: 1, // calculate based of queue length in preloader
        product: 5,
        ui: 5,
    }

    protected updateTexts : boolean = false; // if texts in progress should be updated

    protected texts = {
        // 0 : 'Loading...',
        // 50: 'Half way thru!',
        // 75: 'Almost done..',
        // 90: 'Get ready!',
    }
    
    constructor(_config?){
        
        super(_config || {
            progressContainer: document.querySelectorAll('#customizerProgressBar')[0],
            progressValueContainer: document.querySelectorAll('#customizerProgressValue')[0],
            textContainer: document.querySelectorAll('#customizerProgressText')[0],
        });

        Customizer.app.action.listen(EVENT_CUSTOMIZER_VISIBLE, this.hide.bind(this) );
    }

    protected show() {

        if( Customizer.app.action.exists(EVENT_STATE_LOADING) ){
            Customizer.app.action.call(EVENT_STATE_LOADING, null, null);
        }else{
            document.getElementsByTagName('html')[0].classList.add('customizer-loading');
        }

    }

    protected hide(){
        
        document.getElementsByTagName('html')[0].classList.remove('customizer-loading');
    }

    protected onComplete(){
        Customizer.app.action.call(EVENT_CUSTOMIZER_READY, null, null);       
    }
    

}