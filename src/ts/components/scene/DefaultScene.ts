
import * as THREE from 'three';
import * as THREEGL from 'three/examples/jsm/WebGL';
import { isNumber } from 'util';
import { Customizer } from 'Customizer';

export class DefaultScene{

    protected scene;
    protected camera;
    protected renderer;
    protected container;
    protected canvas;
    protected renderQueue = {};
    protected rendererRunning = false;
    protected loop;
    protected parentContainer : HTMLElement;
    private affectsSize = {
        desktop: [],
        mobile: [],
    }

    protected last = {
        width: 0,
        height: 0,
    } as any;

    constructor(){

       this.prepare();
    }

    protected async prepare(){

        this.last.width = window.innerWidth;
        this.last.height = window.innerHeight;

        this.checkConfigForHeightModifications();

        this.parentContainer = this.setParentContainer();
        this.container = this.createCanvasContainer();
        this.canvas = this.createThreeCanvas();
        this.scene = this.createScene();
        this.renderer = await this.createRenderer();
        this.camera = await this.createCamera();

        this.setBackground();
        this.listen();
        this.windowEvents();
        this.render();
        this.afterPrepare();

        this.onWindowResize(); // trigger resize
    }

    protected afterPrepare(){

    }

    protected init(){
        this.startRenderer();
    }

    protected getScene(){
        return this.scene;
    }

    protected getCamera(){
        return this.camera;
    }

    protected getRenderer(){
        return this.renderer;
    }

    protected getCanvas(){
        return this.canvas;
    }

    protected setBackground(){

    }

    protected windowEvents(){
        window.addEventListener( 'resize', this.onWindowResize.bind(this), false );
    }

    protected setParentContainer(){

        const elemQuery = '.customizerCanvasParent';
        return document.querySelectorAll(elemQuery)[0] as HTMLElement;
    }

    protected animate(){
        this.loop = this.renderer.setAnimationLoop(this.render.bind(this));
    }

    protected triggerRender(){
        this.render();
    }

    private render(){

        this.renderQueued();
        this.renderer.render( this.scene, this.camera );
    }

    protected listen(){
    }   


    private renderQueued(){
        for(let name in this.renderQueue){
            this.renderQueue[name].run();
        }
    }

    // add additional functions to renderer using this + events
    protected addToRenderQueue(...args){

        let name = args[0];
        let run = args[1];

        if( ! this.renderQueue.hasOwnProperty(name) ){
            this.renderQueue[name] = {
                run : run
            }
        }

    }

    protected addToScene(...args){

        for(let i=0;i<args.length;i++){
            this.scene.add(args[i]);
        }
    }

    protected removeFromScene(...args){
        let mesh = args[0];
        if(mesh == undefined) return;

        mesh.geometry.dispose();
        mesh.material.dispose();
        this.scene.remove(mesh);
    }

    protected createCanvasContainer(){

        const container = document.getElementById('customizerCanvasWrapper'); // TODO - thru config
        return container;
        
    }

    protected createThreeCanvas(){

        const canvas = document.createElement('canvas');
        this.container.appendChild(canvas);

        return canvas;
    }

    protected getNavHeight(){

        const nav = document.querySelectorAll('nav')[0];
        if(typeof nav === 'undefined'){
            return 0;
        }

        return nav.clientHeight || nav.offsetHeight;
    }

    protected async getCanvasSizes(){

        let lowerHeight = this.lowersCanvasHeight();
      
        let original = {
            width:  window.innerWidth,
            height: window.innerHeight - lowerHeight,
        }

        let width = original.width;
        let height = original.height;

        let desktopSize = this.parentContainer.dataset.desktopSize || "0.5";
        let mobileSize = this.parentContainer.dataset.mobileSize || "1.0";
        let uiSize = { // no visible no atm
            width: 0,
            height: 0,
        }

        if(window.innerWidth > 768){

            if(desktopSize == 'calc'){ // calculate by subtracting UI width from 100vw

                // if(Customizer.app.action.exists(EVENT_GET_UI_SIZE)){
                //     uiSize = await Customizer.app.action.call(EVENT_GET_UI_SIZE, null, true) as any;
                // }

                width = window.innerWidth - uiSize.width;
                if(width < 1) width = original.width;

            }else if(isNumber(parseFloat(desktopSize))){ // vw * number

                width = window.innerWidth * parseFloat(desktopSize);
                

            }else{ // calc using parent width?
                
            }

        }else{

            // for now mobile size is always 100vw * ratio
            width = window.innerWidth * parseFloat(mobileSize);

        }

        return {
            width: width,
            height: height
        }
    }

    protected checkConfigForHeightModifications(){

        var config = Customizer.app.config.website.get();
        if(config.hasOwnProperty('lowersCanvasHeight')){
            this.affectsSize.desktop = [];
            this.affectsSize.mobile = [];

            // desktop
            for(let i=0;i<config.lowersCanvasHeight.length;i++){
                let elem = document.querySelectorAll( config.lowersCanvasHeight[i] )[0];
                if(elem !== undefined){
                    this.affectsSize.desktop.push(elem);
                }
            }

            // mobile
            for(let i=0;i<config.lowersCanvasHeightMobile.length;i++){
                let elem = document.querySelectorAll( config.lowersCanvasHeightMobile[i] )[0];
                if(elem !== undefined){
                    this.affectsSize.mobile.push(elem);
                }
            }
        }

    }

    protected lowersCanvasHeight(){

        var negativeHeight = 0;
        var affectsSize = window.innerWidth > 800 ? this.affectsSize.desktop : this.affectsSize.mobile;

        if(affectsSize.length > 0){
            for(let i=0;i<affectsSize.length;i++){
                negativeHeight += Math.max( parseInt( affectsSize[i].offsetHeight || affectsSize[i].outerHeight ), 0);
            }
        }

        return negativeHeight;
    }

    protected async createRenderer(){
        
        const rendererParams = <any>{
            antialias: true,
            canvas: this.getCanvas(),
            // logarithmicDepthBuffer: true,
        }

        if ( THREEGL.WEBGL.isWebGL2Available() ){
            rendererParams.context = this.getCanvas().getContext( 'webgl2', { alpha: false } );
        }else{
            rendererParams.context = this.getCanvas().getContext('experimental-webgl2');
        }

        const size = await this.getCanvasSizes();
        rendererParams.canvas.width = size.width;
        rendererParams.canvas.height = size.height;

        const renderer = new THREE.WebGLRenderer(rendererParams);
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( size.width, size.height );
        renderer.gammaOutput = true;
        renderer.gammaFactor = 1.2;

        renderer.context.canvas.addEventListener('webglcontextlost', this.webglContextError);

        return renderer;
    }

    protected matchCanvasHeightOnElements(height){
        var elements = document.querySelectorAll('[data-match-height]');
        if(elements.length > 0){
            for(let i=0;i<elements.length;i++){
                //@ts-ignore
                elements[i].style.height = height+'px';
            }
        }
    }

    protected setBackgroundColor(){

    }

    protected startRenderer(){

		if(this.rendererRunning){
			return; // already running
		}

		this.rendererRunning = true;
		this.animate();
    }

    protected stopRenderer(){
        
        if( ! this.rendererRunning) return; // already paused

		cancelAnimationFrame(this.loop);
		this.rendererRunning = false;
    }

    protected webglContextError(event: Event){
        event.preventDefault();
        console.warn('CRITICAL ERROR', event);
        alert('Unfortunately WebGL has crashed. Please reload the page to continue!');
    }


    protected createScene(){
        return new THREE.Scene();
    }

    protected async createCamera(){
        
        const cameraInitialPosition = new THREE.Vector3(0,75,700);

        const size = await this.getCanvasSizes();

        const camera = new THREE.PerspectiveCamera( 30, size.width / size.height, 1, 10000 );
        camera.position.copy(cameraInitialPosition);

        return camera;
    }


    private async onWindowResize(...args) {

        // if only height changes 
        let disableResize = false;
        if(window.innerWidth < 800){
            if( this.last.height !== window.innerHeight && this.last.width === window.innerWidth){
                disableResize = true;
            }
        }

        if(disableResize){
            console.log('Resize canvas canceled on mobile');
            return;
        }

        const forcedWidth = args[0], 
              forcedHeight = args[1];
        
        let size = await this.getCanvasSizes();

        if(typeof forcedWidth !== 'undefined' && forcedWidth > 0) size.width = forcedWidth;
		if(typeof forcedHeight !== 'undefined' && forcedHeight > 0) size.height = forcedHeight;

        this.canvas.width = size.width;
        this.canvas.height = size.height;
        this.renderer.setSize( size.width, size.height );
        this.camera.aspect = size.width / size.height;
        this.camera.updateProjectionMatrix();
       

        this.matchCanvasHeightOnElements(size.height);
    }


}