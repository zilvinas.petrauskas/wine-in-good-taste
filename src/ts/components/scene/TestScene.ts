import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as THREE from 'three';
import { BoxBufferGeometry } from 'three';
import { Customizer } from 'Customizer';
import { EVENT_ADD_TO_SCENE, EVENT_ADD_TO_RENDERER } from 'components/constants';

export class TestScene{


    constructor(){
        this.init();
    }

    protected init(){

        const renderer = new THREE.WebGLRenderer();

        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( renderer.domElement );
        const scene = new THREE.Scene();
        scene.background = new THREE.Color( 0x222222 );
        
        const camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );
        camera.position.set( 0, 0, 500 );
        
        const controls = new OrbitControls( camera, renderer.domElement );
        controls.minDistance = 200;
        controls.maxDistance = 500;
        scene.add( new THREE.AmbientLight( 0x222222 ) );
        
        var light = new THREE.PointLight( 0xffffff );
        light.position.copy( camera.position );
        scene.add( light );
        
        
        var geometry = new THREE.BoxBufferGeometry( 200, 200, 200 );
        var material = new THREE.MeshBasicMaterial({
            color: new THREE.Color('white'),
            wireframe: true,
        });
        var mesh = new THREE.Mesh( geometry, material );
        scene.add(mesh);

        let clock = new THREE.Clock();

        animate();

        
        function rotateDummyBox(){
            var delta = clock.getDelta() * 0.5;
                mesh.rotation.x += delta;
                mesh.rotation.y += delta;
        }

        function animate(){
            requestAnimationFrame( animate );
            rotateDummyBox();
            controls.update();
            renderer.render( scene, camera );
        }
    }
}