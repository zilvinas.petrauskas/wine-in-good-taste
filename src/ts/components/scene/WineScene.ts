import { DefaultScene } from './DefaultScene';
import { Customizer } from 'Customizer';
import { EVENT_ADD_TO_SCENE, EVENT_REMOVE_FROM_SCENE, EVENT_GET_THREE_SCENE, EVENT_GET_THREE_RENDERER, EVENT_GET_THREE_CAMERA, EVENT_ADD_TO_RENDERER, EVENT_GET_THREE_CANVAS, EVENT_GET_GROUND_CONFIG, EVENT_TRIGGER_RENDER, EVENT_GET_CANVAS_SIZE, EVENT_CUSTOMIZER_SCENE_READY, EVENT_PAYLOAD_FINISH, EVENT_GET_CAMERA_INITIAL_POSITION } from 'components/constants';
import { Color } from 'three';

export class WineScene extends DefaultScene{


    protected afterPrepare(){

        Customizer.app.action.broadcast(EVENT_CUSTOMIZER_SCENE_READY, null);
        Customizer.app.action.listen(EVENT_PAYLOAD_FINISH, this.startRenderer.bind(this));

        this.setBackgroundColor();
    }

    protected listen(){
        
        Customizer.app.action.register(EVENT_ADD_TO_SCENE, this.addToScene.bind(this) );
        Customizer.app.action.register(EVENT_REMOVE_FROM_SCENE, this.removeFromScene.bind(this) );
        Customizer.app.action.register(EVENT_GET_THREE_SCENE, this.getScene.bind(this) );
        Customizer.app.action.register(EVENT_GET_THREE_CAMERA, this.getCamera.bind(this) );
        Customizer.app.action.register(EVENT_GET_THREE_RENDERER, this.getRenderer.bind(this) );
        Customizer.app.action.register(EVENT_GET_THREE_CANVAS, this.getCanvas.bind(this) );
        Customizer.app.action.register(EVENT_ADD_TO_RENDERER, this.addToRenderQueue.bind(this) );
        Customizer.app.action.register(EVENT_GET_CANVAS_SIZE, this.getCanvasSizes.bind(this));

        Customizer.app.action.listen(EVENT_TRIGGER_RENDER, this.triggerRender.bind(this) );

    }

    
    protected createThreeCanvas(){

        const canvas = document.createElement('canvas');
        canvas.setAttribute('id','three-canvas');
       
        // transparent BG - fix black bg on ios + ipad + safari
        const gl = canvas.getContext("webgl", { premultipliedAlpha: false });
        gl.clearColor(1, 1, 1, 0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        // end of transparent bg fix

        this.container.appendChild(canvas);

        return canvas;

    }

    // transparent bg initiated within createThreeCanvas() 
    protected setBackgroundColor(){

        this.scene.background = new Color(0xf5f1ee);
        // this.scene.background = new Color(0x09070a);

    }

    protected setParentContainer(){

        const elemQuery = '#customizerCanvasParent';
        return document.querySelectorAll(elemQuery)[0] as HTMLElement;
    }

    // protected async createCamera(){
        
    //     const cameraInitialPosition = new THREE.Vector3(0,75,700);

    //     const size = await this.getCanvasSizes();

    //     const camera = new THREE.PerspectiveCamera( 30, size.width / size.height, 1, 10000 );
    //     camera.position.copy(cameraInitialPosition);

    //     return camera;
    // }

}