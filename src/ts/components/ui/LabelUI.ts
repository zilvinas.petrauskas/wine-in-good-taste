import { Customizer } from 'Customizer';
import { EVENT_CUSTOMIZE_STEP_1, EVENT_CUSTOMIZE_STEP_2, EVENT_LABEL_UI_STEP_1, EVENT_CREATE_MASK_CANVAS, EVENT_UPDATE_MODEL_LABEL, EVENT_DESTROY_MASK_CANVAS, EVENT_LABEL_IMAGE_SCALING, EVENT_GET_PART_TO_CUSTOMIZE, EVENT_RESET_SCALE, EVENT_CUSTOMIZE_HIDDEN, EVENT_CUSTOMIZE_UPLOAD, EVENT_UPDATE_LABEL_MATERIAL, EVENT_UPDATE_MATERIAL, EVENT_UPDATE_2D_CANVAS_IMAGE, EVENT_GET_LABEL_IMAGE, EVENT_GET_LABEL_CANVAS, EVENT_ADJUST_CANVAS } from 'components/constants';
import { LabelCanvas } from 'components/material/LabelCanvas';
import { Part } from 'components/model/Part';

const STEP_1 = 'customizer-step-1';
const STEP_2 = 'customizer-step-2';
const PREVENT_SCROLL = 'prevent-scrolling';

export class LabelUI{

    private step : number = 0;
    private currentPart;
    private labelCanvas: any;
    private wasCustomized = {
        upload : false,
        preview : false,
        approved: false,
    }

    constructor(){

        this.prepare();
        this.listen();
        this.bindEvents();
    }

    protected listen(){

        Customizer.app.action.register(EVENT_CUSTOMIZE_HIDDEN, this.hide.bind(this));
        Customizer.app.action.register(EVENT_CUSTOMIZE_STEP_1, this.step1.bind(this));
        Customizer.app.action.register(EVENT_CUSTOMIZE_STEP_2, this.step2.bind(this));
        Customizer.app.action.register(EVENT_CUSTOMIZE_UPLOAD, this.uploadImage.bind(this));
        Customizer.app.action.register(EVENT_UPDATE_MODEL_LABEL, this.previewLabel.bind(this));

        Customizer.app.action.register(EVENT_RESET_SCALE, this.resetScaler.bind(this));
        Customizer.app.action.register(EVENT_GET_PART_TO_CUSTOMIZE, this.getPartToCustomize.bind(this));


    }

    protected prepare(){
        this.labelCanvas = new LabelCanvas();
    }

   

    protected bindEvents(){

        document.querySelectorAll('#finishProductPreview')[0].addEventListener('click', ()=>{
            Customizer.app.action.call(EVENT_CUSTOMIZE_HIDDEN);
            scope.updateCustomizationStatus('approved', true);
            this.adjustPriceIfCustomized();
        });

        document.querySelectorAll('#uploadCustomImage')[0].addEventListener('click', (event)=>{
            this.uploadImage();
        });

        const scope = this;
        document.querySelectorAll('#uploadFile')[0].addEventListener('change', async (e)=>{
           
            await scope.updateCanvas(e, (success)=>{
                console.log('uploaded?', success);
                if(success) scope.updateCustomizationStatus('upload', true);
            });

        });

        document.querySelectorAll('.product-plus-sign')[0].addEventListener('click', ()=>{
            Customizer.app.action.call(EVENT_CUSTOMIZE_STEP_2);
        })

        document.querySelectorAll('#previewModifiedLabel')[0].addEventListener('click', ()=>{
            this.previewLabel();
            Customizer.app.action.call(EVENT_CUSTOMIZE_STEP_1);
            scope.updateCustomizationStatus('preview', true);
        })


        const sliderElem = document.querySelectorAll('#scaleLabelImage')[0] as HTMLInputElement;
        sliderElem.addEventListener('change', (e: any)=>{
            console.log('scale', e.target.value);
            triggerScaleChange(e.target.value);
        });


        // bind slider up + down
        document.querySelectorAll('#sliderDown')[0].addEventListener('click', (e: any)=>{
            changeSliderValue(-parseFloat(sliderElem.getAttribute('step')));
        });

        document.querySelectorAll('#sliderUp')[0].addEventListener('click', (e: any)=>{
            changeSliderValue(parseFloat(sliderElem.getAttribute('step')));
        });


        function triggerScaleChange(value){
            Customizer.app.action.call(EVENT_LABEL_IMAGE_SCALING, null, null, value || 1);
        }

        function changeSliderValue(change){
            
            var newValue = parseFloat(sliderElem.value) + change;

            var min = parseFloat(sliderElem.getAttribute('min'));
            var max = parseFloat(sliderElem.getAttribute('max'));
            if(newValue < min) newValue = min;
            if(newValue > max) newValue = max;

            sliderElem.value = newValue;

            triggerScaleChange(newValue);
        }
    }
    

    protected resetScaler(){
        const scaleSlider = document.querySelectorAll('#scaleLabelImage')[0] as any;
        scaleSlider.value = 1;
    }

    protected hide(){

        this.step = 0;
        document.body.classList.remove(STEP_1);
        document.body.classList.remove(STEP_2);
        document.querySelectorAll('html')[0].classList.remove(PREVENT_SCROLL);
    }

    // show plus sign (preparation for 2d cusotmizer)
    protected step1(){

        this.step = 1;
        // show extras on screen

        Customizer.app.action.broadcast(EVENT_LABEL_UI_STEP_1);

        document.body.classList.add(STEP_1);
        document.querySelectorAll('html')[0].classList.add(PREVENT_SCROLL);

        document.body.classList.remove(STEP_2);
        
        

    }

    // show 2d label customizer
    protected step2(){

        let dummyConfig = [{ key: 'front'}];
        this.currentPart = dummyConfig[0].key;
        // Need better way to store current part...
        // I'm not sure yet, would like to talk to you about it
        
        document.body.classList.add(STEP_2);
        document.querySelectorAll('html')[0].classList.add(PREVENT_SCROLL);

        document.body.classList.remove(STEP_1);

        Customizer.app.action.call(EVENT_CREATE_MASK_CANVAS); // qq5 - its called too early on first run??

        this.step = 2;
        // display 2d customizer

    }

    // upload custom image to canvas

    protected uploadImage() {

        let uploadFile = document.querySelectorAll('#uploadFile')[0];
        //@ts-ignore
        uploadFile.click(); 

    }

    protected hidePlaceholder(hide){
        var elem = document.querySelectorAll('.label-customizer-wrapper')[0];
        if(hide)
            elem.classList.remove('blank');
        else
            elem.classList.add('blank');
        
    }

    // update 2d canvas image with new uploaded image
    protected async updateCanvas(e: any, callback: any = null) {

        if(e.target.files.length === 0){
            this.hidePlaceholder(false);
            alert('Select and upload an image');
            return;
        }


        const scope = this;

        //@ts-ignore
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.onload = async function() {

            let img = new Image();
            img.crossOrigin = "anonymous";
            //@ts-ignore
            img.src = reader.result;
            img.onload = async function() {

                let [passed, error] = scope.checkUploadRequirements(img);

                if(!passed){
                    alert(error);
                    if(callback) callback(passed);
                    return;
                }

                await Customizer.app.action.call(EVENT_UPDATE_2D_CANVAS_IMAGE, null, null, img, true);
                scope.hidePlaceholder(true);

                // BUG FIX for first run?
                await Customizer.app.action.call(EVENT_UPDATE_MODEL_LABEL, null, null);
                await Customizer.app.action.call(EVENT_CREATE_MASK_CANVAS, null, null);
                

                if(callback) callback(passed);
            }
        }
        reader.readAsDataURL(file);
    }

    protected checkUploadRequirements(img){

        let passed = true;
        let error = '';
        const config = Customizer.app.config.product.get(); 
        if(config.hasOwnProperty('upload')){
            if(config.upload.hasOwnProperty('min')){
                const min = config.upload.min;
                if( min.width > 0 && img.width < min.width){
                    error = 'Minimum image width should be at least '+min.width+'px';
                    passed = false;
                }
                if(min.height > 0 && img.height < min.height){
                    error = 'Minimum image height should be at least '+min.height+'px';
                    passed = false;
                }
            }
        }

        return [passed, error];
    }
    

    protected async previewLabel(){


        let dummyConfig = [{ key: 'front'}];

        let labelImage = await Customizer.app.action.call(EVENT_GET_LABEL_CANVAS);
        // goes to Model.ts updatePartsMaterials()
        await Customizer.app.action.call(EVENT_UPDATE_LABEL_MATERIAL, null, null, dummyConfig, labelImage);
      
        Customizer.app.action.call(EVENT_DESTROY_MASK_CANVAS);// this ONLY works on 2nd upload or after preview button click

    }

    protected async getPartToCustomize() {

        return this.currentPart;

    }


     // calls fn in shopify to update price (adds $ to base price when product is customized)
     protected adjustPriceIfCustomized(){

        const customized = this.wasCustomized.upload && this.wasCustomized.preview && this.wasCustomized.approved;
        window.dispatchEvent( new CustomEvent('variant:customized', { detail: customized }) );
    }

    protected updateCustomizationStatus(key, value){
        this.wasCustomized[key] = value;
    }

}