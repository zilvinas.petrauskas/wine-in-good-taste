import { EVENT_CUSTOMIZER_VISIBLE, EVENT_CUSTOMIZER_READY, EVENT_CUSTOMIZE_STEP_1, EVENT_PROGRESS_ADD } from 'components/constants';
import { Customizer } from 'Customizer';
import { SmoothScroll } from 'components/helpers/smoothScroll';

export class UI {


    constructor() {
        this.prepare();
        this.listen();
    }

    protected listen() {

        Customizer.app.action.register(EVENT_CUSTOMIZER_VISIBLE, this.show.bind(this));
        Customizer.app.action.listen(EVENT_CUSTOMIZER_READY, this.events.bind(this));

        Customizer.app.action.call(EVENT_PROGRESS_ADD, null, null, 'ui');
    }

    protected prepare(){
        this.checkForIphone();
    }

    protected checkForIphone(){

        var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

        if(iOS){
            document.body.classList.add('iphone');
        }
    }

    public show() {

        document.getElementsByTagName('html')[0].classList.add('customizer-loaded');

    }


    protected events() {

        const scope = this;

        const smoothScroll = SmoothScroll({
            header: '#site-header'
        });
        console.log('SMOT', smoothScroll);

        // step 1 button
        const btn = document.querySelectorAll('#customizeProduct')[0];
        btn.addEventListener('click', () => {

            if (window.innerWidth < 800) {
                smoothScroll.to(0);
            }else{
                window.scrollTo(0,0);
            }

            Customizer.app.action.call(EVENT_CUSTOMIZE_STEP_1);
        });

    }

    // protected scrollTopOnMobile(){

    //     if (window.innerWidth < 800) {
    //         window.scroll({
    //             top: 0, 
    //             behavior: 'smooth'
    //         });
    //     }

    // }


}