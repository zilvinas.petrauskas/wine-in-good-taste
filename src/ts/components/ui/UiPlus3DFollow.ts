import { Vector3, Object3D, Camera, Color, MeshBasicMaterial, BoxBufferGeometry, Mesh } from 'three';
import { Customizer } from 'Customizer';
import { EVENT_GET_THREE_CAMERA, EVENT_GET_CANVAS_SIZE, EVENT_ADD_TO_SCENE, EVENT_MOUSE_PRESS, EVENT_MOUSE_RELEASE, EVENT_GET_THREE_CANVAS, EVENT_LABEL_UI_STEP_1, EVENT_AFTER_CONTROLS_UPDATE, EVENT_CUSTOMIZER_READY, EVENT_STATE_VISIBLE, EVENT_CUSTOMIZER_VISIBLE } from 'components/constants';
import { GUI } from 'three/examples/jsm/libs/dat.gui.module';

// adds a dot on the top left corner of the bottle for html element to follow
export class UiPlus3DFollow{

   
    protected position : Vector3;
    protected container : any;
    protected dot : Object3D;
    protected camera : Camera;
    protected canvas : HTMLElement;
    protected gui : GUI;

    private debug = false; // creates admin gui for position change

    constructor(_config: any = {}){

       this.position = _config.position || new Vector3(-49,33,0);
       this.container = _config.container || document.querySelectorAll('.product-plus-sign')[0]

       this.init();
    }

    protected async init(){

        const scope = this;
        this.createDot();
        this.camera = await Customizer.app.action.call(EVENT_GET_THREE_CAMERA, null, true) as Camera;
        this.canvas = await Customizer.app.action.call(EVENT_GET_THREE_CANVAS, null, true) as HTMLElement;
              
        if(window.innerWidth < 800) Customizer.app.action.listen(EVENT_CUSTOMIZER_VISIBLE, this.updatePosition.bind(this));
        Customizer.app.action.listen(EVENT_LABEL_UI_STEP_1, this.updatePosition.bind(this));
        Customizer.app.action.call(EVENT_AFTER_CONTROLS_UPDATE, null, null, this.updatePosition.bind(this));

        window.addEventListener('resize', this.resize.bind(this));

        if(this.debug) this.admin();

        
    }

    protected resize(){
        this.updatePosition();
    }

    protected createDot(){

        var size = 2;
        var geometry = new BoxBufferGeometry( size, size, size); 
        var material = new MeshBasicMaterial( { color: new Color('red') } ); 
        
        this.dot =new Mesh( geometry, material ); 
        this.dot.position.copy( this.position );
        this.dot.visible = this.debug;
        
        Customizer.app.action.call(EVENT_ADD_TO_SCENE, null, null, this.dot);
    }

    protected toggleDotVisibility(visible){
        this.dot.visible = visible;
    }

    protected async updatePosition(){

        let pos = await this.toScreenPosition(this.dot) as any;

        let x = pos.x - (this.container.offsetWidth / 2);
        let y = pos.y - (this.container.offsetHeight / 2);

        this.container.style.left = x + 'px';
        this.container.style.top = y + 'px';
    }


    private admin(){

        this.createGui();
        this.toggleDotVisibility(true);
    }

    private createGui(){

        console.log('creating gui');

        const scope = this;
        this.gui = new GUI();

        this.gui.add(scope.position, 'x').onChange(function(val){
            scope.position.x = val;
            scope.dot.position.copy( scope.position );
        });
        this.gui.add(scope.position, 'y').onChange(function(val){
            scope.position.y = val;
            scope.dot.position.copy( scope.position );
        });

        this.gui.open();


        const guiElem = document.querySelectorAll('.dg.ac')[0];
        guiElem.classList.add('dotStyle1');
    }

    protected async toScreenPosition(object){

        let pos = new Vector3();
        pos = pos.setFromMatrixPosition(object.matrixWorld);
        pos.project(this.camera);

        let canvasSize = await this.getCanvasSize() as any;

        let widthHalf = canvasSize.width / 2;
        let heightHalf = canvasSize.height / 2;

        pos.x = (pos.x * widthHalf) + widthHalf;
        pos.y = - (pos.y * heightHalf) + heightHalf;
        pos.z = 0;

        return new Promise(resolve => resolve(pos));
    }

    protected async getCanvasSize(){
        let size = await Customizer.app.action.call(EVENT_GET_CANVAS_SIZE, null, true);
        return new Promise(resolve =>resolve(size));
    }

}