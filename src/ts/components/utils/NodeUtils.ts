import * as NODES from 'three/examples/jsm/nodes/Nodes';
import * as THREE from 'three';
import { hexToRGB, rgbToHex } from 'components/helpers/helpers';

export class NodeUtils{

   

	public static verifyColor(hex1, hex2, tolerance) {

		return this.colorDifference(hex1, hex2) < tolerance;

	}
	

	public static colorDifference (hex1, hex2) {

		var h1 = hexToRGB(hex1);
		var h2 = hexToRGB(hex2);

		var sumOfSquares = 0;
	
		sumOfSquares += Math.pow(h1.r - h2.r, 2);
		sumOfSquares += Math.pow(h1.g - h2.g, 2);
		sumOfSquares += Math.pow(h1.b - h2.b, 2);
		
		let diff = Math.sqrt(sumOfSquares);
		
		return diff;
	}
    
    

    public static analyzeImgData = (imgData: Uint8ClampedArray) =>{

		const arrayOfColors = [];

		for (let i = 0; i < imgData.length; i += 4){
			const r = imgData[i];
			const g = imgData[i + 1];
			const b = imgData[i + 2];
			const a = imgData[i + 3];

			if (a === 255){
				const hex = rgbToHex(r, g, b);
				if (!arrayOfColors.includes(hex)){
					arrayOfColors.push(hex);
				}
			}
		}

		return arrayOfColors;
	};
	
	public static colorToAlpha(inputNode, color, tolerance?) {

		var t;

		if (tolerance != undefined) {
			t = tolerance.toString()
		} else {
			t = "0.015";
		}

		let colorSelector = new NODES.FunctionNode(
			//@ts-ignore
			[
				"vec4 alpha (sampler2D image, vec4 color, vec2 uv ) {",
				"	vec4 a;",
				"	float e = " + t + ";",
				"	vec3 eps = vec3(e,e,e);",
				
				"	vec4 t = texture2D(image, uv);",
				"	a = vec4(0.0, 0.0, 0.0, 1.0);",
				"	if (all(greaterThanEqual(t, vec4(color.rgb-eps, 1.0-e))) && all(lessThanEqual(t, vec4(color.rgb+eps, 1.0))))",
				"		a = vec4 (1.0, 1.0, 1.0, 1.0);",
				"	return a;",
				"}"
			].join( "\n" ) );




		var colorCall = new NODES.FunctionCallNode( colorSelector, {
			//@ts-ignore
			image: inputNode,
			color: new NODES.ColorNode('#' + color),
			uv : inputNode.uv
		} );
		
		return colorCall;
		

	}
    
    public static readCanvasColors(canvas, ctx){

        const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
        let arrayOfColors = NodeUtils.analyzeImgData(imageData);
        const colors = {};

		for (let i = 0; i < arrayOfColors.length; i++) {
			const maskColor = arrayOfColors[i];
			colors[i] = maskColor;
		}

        return colors;
	};

	public static alphaBlendNodes (baseNode, inputNode, alphaNode) {

		// to blend stitch alpha with mask alpha
		let blend = new NODES.MathNode(
				baseNode,
				inputNode,
				alphaNode,
				NODES.MathNode.MIX
			);

		return blend;

	}
	
	public static addNodes(input1, input2) {
		// to overlay node	
	
		let add = new NODES.OperatorNode(
			input1,
			input2,
			NODES.OperatorNode.ADD	
		);

		return add;


	}

	public static subNodes(input1, input2) {
		// to remove certain section from node	
	
		let sub = new NODES.OperatorNode(
			input1,
			input2,
			NODES.OperatorNode.SUB	
		);
		return sub;

	}
	
	public static normalNodeAdjustment(baseNode, inputNode, alphaNode) {
		// replace black with grey, since grey is (0,0,0) on normal coordinate
		const grey = new NODES.ColorNode(new THREE.Color(0x8080FF));
		const base = baseNode ? baseNode : grey;

		let normalNode = new NODES.MathNode(
			base,
			inputNode,
			alphaNode,
			NODES.MathNode.MIX
		);

		return normalNode;

	}
    
}