
export class Component{

    private components = {};

    /*
        arguments
        [0] - component name
        [1] - component class
    */
    public async add(...args){

        const _name = args[0];
        const _class = args[1];

        return new Promise(resolve => {
            if( this.components.hasOwnProperty(_name) ){
                console.log('Component already exists: '+_name);
                return;
            }
            this.components[_name] = _class;
            resolve(_class);
        });
    }

}