
import { Customizer } from 'Customizer';
import { DefaultConfig } from './DefaultConfig';
import { ALL_CONFIGS_LOADED } from 'components/constants';

export class Config extends DefaultConfig{

    protected async init() {

        await this.websiteConfig();
        await this.productConfig();
        await this.modelConfig();
        await this.uiConfig();
        await this.exporterConfig();

        this.completed();
    }
    
    protected completed() {
        Customizer.app.action.call(ALL_CONFIGS_LOADED);
    }

}
