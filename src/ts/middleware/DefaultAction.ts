export class DefaultAction{

    protected actions = {};
    protected listeners = {};
    protected debug: boolean = false; // writes all actions into console.log

    constructor(actionList){

        if(typeof actionList !== 'undefined' && Array.isArray(actionList) && actionList.length > 0){
            for(let i=0;i<actionList.length;i++){
                let action = actionList[i];
                this.register(action.name, action.run, action.params);
            }
        }
    }

    public register(eventName, callback, _options?){

        const options = {
            unique : true,
        }

        if(typeof _options !== 'undefined' && _options !== null){
            for(let o_O in _options) options[o_O] = _options[o_O];
        }


        // if event should be unique (only one with that name)
        if( options.unique && this.actions.hasOwnProperty(eventName) ){
            console.warn('Event "'+eventName+'" is already defined', this.actions[eventName]);
            return;
        }

        if( ! options.unique){
            if( ! this.actions.hasOwnProperty(eventName)) this.actions[eventName] = [];
            this.actions[eventName].push(eventParams());
        }else{
            this.actions[eventName] = eventParams();
        }

        function eventParams(){
            return {
                name: eventName,
                fn: (...args) => callback(...args), // this.testChange
                callback: callback,
                options: options,
            }
        }

    }

    public async call(eventName, _options, callback2, ...args){ // ..args = [1,2,23,3,4,5,,6]

        if(this.debug){
            
            if(eventName !== 'EVENT_GET_CANVAS_SIZE' && eventName !== 'EVENT_GET_UI_SIZE'){
                console.log('- event call', eventName, ...args);
            }

        }

        const options = {
            passTriggerCallback: false, // instead of passing arguments pass a callback2 with arguments - used for 
        }
        if(typeof _options !== 'undefined' && _options !== null){
            for(let o_O in _options) options[o_O] = _options[o_O];
        }

        if( ! this.actions.hasOwnProperty(eventName) ){
            console.warn('- event.trigger - Cant trigger an event that doesnt exist', eventName);
            return;
        }

        let sent = false;
        let events = this.actions[eventName];
        let eventsArray = Array.isArray(events) ? [...events] : [events];

        for(let i=0;i<eventsArray.length;i++){

            let event = eventsArray[i];

            if(eventsArray.length > 1){ // for example, assign parts. Its rare to have non unique events

                let completed = event.fn(...args); 

                if(typeof callback2 == 'function'){
    
                    sent = true;
                    callback2(completed); 

                }else if( callback2 === null){
                    // cool, do not return anything, just trigger

                }else{
                    console.warn('Multi-event callback should be either a function or a null (silent) ', typeof callback2, callback2);
                }

            }else{

                if(options.passTriggerCallback === true){

                    let completed = event.callback((...args)=>callback2(...args));
    
                    return new Promise(resolve => {
                        resolve(completed);
                    })
    
                }else{
    
                    let completed = await event.fn(...args); 
    
                    return new Promise(resolve => {
    
                        if(typeof callback2 == 'function'){
    
                            sent = true;
                            callback2(completed); 
                            resolve(completed);
    
                        }else if(callback2 === true){ 
    
                            sent = true;
                            resolve(completed);
    
                        }else if(this.listeners.hasOwnProperty(eventName)){ // resolves with broadcasting included
                        
                            this.broadcast(eventName, completed);
                        }
    
                        resolve(completed);
    
                    })
                }

            }

           

        }

        if(eventsArray.length > 1){
            return new Promise(resolve => {
                resolve();
            })
        }

    }

    public listen(eventName, callback){

        // when some event is triggered

        if( ! this.listeners.hasOwnProperty(eventName) ){
            this.listeners[eventName] = [];
        }

        this.listeners[eventName].push({
            fn: (...args) => callback(...args),
        });

    }

    public broadcast(eventName, args){

        if(this.debug)  console.log('- broadcast', eventName);
        
        if( this.listeners.hasOwnProperty(eventName) ){
            for(let i=0;i<this.listeners[eventName].length;i++){
                this.listeners[eventName][i].fn(args); // trigger saved callbacks with arguments (optionally)
            }
        }

    }

    public remove(name){
        if( this.actions.hasOwnProperty(name) )
            this.actions[name] = undefined;
    }

    public exists(eventName){
        return this.actions.hasOwnProperty(eventName);
    }

    public defaultOptions(_options){
        const options = {
            unique: true,
        }
        if(typeof _options !== 'undefined' && _options !== null){
            for(let opt in _options) options[opt] = _options[opt];
        }
        return options;
    }

    
    protected saveAction(name, callback, options){
        return {
            name: name,
            run: (...args) => callback(...args), // this.testChange
            callback: callback,
            options: options,
        }
    }

    protected isUnique(name="", options){
        if(name == ""){
            return options.unique;
        }
        return options.unique && this.actions.hasOwnProperty(name);
    }
    
    protected parseOptions(options, parseOptions?){

        if(typeof parseOptions !== 'undefined' && parseOptions !== null){
            for(let opt in parseOptions) options[opt] = parseOptions[opt];
        }

        return options;
    }
  

}