import { ConfigLoader } from 'components/config/ConfigLoader';

declare const WEBSITE_CONFIG;
declare const ACTIVE_PRODUCT;
declare const MODEL_CONFIG;
declare const UI_CONFIG;
declare const EXPORTER_CONFIG;

export class DefaultConfig{

    public website; 
    public product; 
    public model; 
    public ui; 
    public exporter; 

    constructor(){
        this.init();
    }

    protected async init() {
        // call configs, whatever is needed
    }

    protected async websiteConfig() {

        let config = {};
        if(typeof WEBSITE_CONFIG !== 'undefined') config = WEBSITE_CONFIG;

        this.website = new ConfigLoader(config);
        await this.website.init();
    }

    protected async productConfig() {

        let config = {};
        if(typeof ACTIVE_PRODUCT !== 'undefined') config = ACTIVE_PRODUCT;
        this.product = new ConfigLoader(config);
        await this.product.init();
    }

    protected async modelConfig() {

        let config = {};
        if(typeof MODEL_CONFIG !== 'undefined') config = MODEL_CONFIG;
        this.model = new ConfigLoader(config);
        await  this.model.init();
    }

    protected async uiConfig() {

        let config = {};
        if(typeof UI_CONFIG !== 'undefined') config = UI_CONFIG;
        this.ui = new ConfigLoader(config);
        await this.ui.init();
    }

    protected async exporterConfig(){

        let config = {};
        if(typeof EXPORTER_CONFIG !== 'undefined') config = EXPORTER_CONFIG;
        this.exporter= new ConfigLoader(config);
        await this.exporter.init();
    }

    protected completed(){
        // call whatever you want after configs are loaded
    }
}