import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import * as THREE from 'three';
import { getExternalAssetsUrl, getBaseUrl } from 'components/helpers/helpers';
import { TYPE_TEXTURE, TYPE_IMAGE, TYPE_GLB } from 'components/constants';

export class DefaultPayload{

    protected queue = [];
    protected loaded = {};
    protected completed = false;

    constructor(){
        this.init();
    }

    protected init(){

    }

    public add(name: string, url: string, type: string, isExternal: boolean = false){

        let fullUrl = isExternal ? getExternalAssetsUrl() + url : getBaseUrl() + url;

        this.queue.push({
            name: name,
            type: type,
            url: fullUrl,
        });

    }

    public async start(){
        return await this.load();
    }

    protected async load(){

        await this.onLoadStart();

        for(let i=0;i<this.queue.length;i++){
            await this.parseItemsByType(this.queue[i], this.onItemLoad.bind(this));
        }

        this.finish();
    }

    protected async parseItemsByType(item, onLoad){

        if(item.type == TYPE_TEXTURE){

            await this.loadTexture(item.url)
            .then((loadedItem)=>{
                onLoad(item.name, loadedItem);
            })
            .catch((err)=>{
                console.warn('item not loaded', err);
            });

        }else if(item.type == TYPE_IMAGE){

            await this.loadImage(item.url)
            .then((image) =>{
                onLoad(item.name, image);
            })
            .catch((err)=>{
                console.warn('Image failed to load', err);
            })

        }else if(item.type == TYPE_GLB){

            // load GLB model with THREE loader
            await this.loadGlb(item.url)
            .then((loadedItem: GLTF) =>{
                onLoad(item.name, loadedItem.scene);
            })
            .catch((err)=>{
                console.warn('GLB file not loaded', err);
            })

        }else{
            console.warn('- unkonwn type at preloader', item.type, item);
        }

    }

    protected async onLoadStart(){
        return new Promise(resolve => resolve());
    }

    protected onItemLoad(itemName, loadedItem){
        this.loaded[itemName] = loadedItem;
    }

    protected async loadGlb(url: string){

        const loader = new GLTFLoader();

        return new Promise((resolve, reject) => {

            loader.load(url, (gltf: GLTF) => {
            
                resolve(gltf);

            }, (progressEvent)=>{
                
            });

        })

		
    }


    protected async loadImage(url: string){

        return new Promise((resolve, reject)=>{

            const img = new Image();
            img.crossOrigin = "anonymous";
            img.onload = ()=>{
                resolve(img);
            }
            img.onerror = reject;
            img.src = url;

        });

    }

    public loadTexture(url: string) {

		return new Promise((resolve, reject) => {
			const loader = new THREE.TextureLoader();
			loader.load(
				url,
				(texture) => {
					resolve(texture)
				},
				undefined,
				(err) => {
                    console.warn('Texture failed to load', url, err);
					reject();
				}
			)
		});
	}

    protected finish(){
        this.completed = true;
    }

    protected getList(){
        return this.loaded;
    }
   
    protected callback(){
        return this.getList();
    }

}