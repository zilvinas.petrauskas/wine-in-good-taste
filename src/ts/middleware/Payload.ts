import { DefaultPayload } from './DefaultPayload';
import { Customizer } from 'Customizer';
import { EVENT_PAYLOAD_FINISH, EVENT_PAYLOAD_LIST, EVENT_PROGRESS_ADD, EVENT_PROGRESS_BAR_STARTED, EVENT_ADJUST_PROGRESS_CONFIG } from 'components/constants';

// class to load textures
export class Payload extends DefaultPayload{

   
    protected init(){

        Customizer.app.action.register(EVENT_PAYLOAD_FINISH, this.callback.bind(this));
        Customizer.app.action.register(EVENT_PAYLOAD_LIST, this.getList.bind(this));
    }

    protected async onLoadStart(){

        await Customizer.app.action.call(EVENT_ADJUST_PROGRESS_CONFIG, null, null, 'preloader', this.queue.length);
        await Customizer.app.action.call(EVENT_PROGRESS_BAR_STARTED, null, null);
        return new Promise(resolve => resolve());
    }

    protected onItemLoad(itemName, loadedItem){

        this.loaded[itemName] = loadedItem;
        Customizer.app.action.call(EVENT_PROGRESS_ADD, null, null, 'preloader');
    }

    protected finish(){

        this.completed = true;
        Customizer.app.action.call(EVENT_PAYLOAD_FINISH, null, null, this.loaded);
        
    }

}