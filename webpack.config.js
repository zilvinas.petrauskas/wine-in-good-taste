
const BUILD_DEV  = "build/dev";
const BUILD_PRODUCTION = 'build/production';

const TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = (env, argv) => {
	
	const production = !!(argv.mode == "production");
	const BUILD_PATH = production ? __dirname + `/${BUILD_PRODUCTION}` : __dirname + `/${BUILD_DEV}`;

	var config = {
		mode: production ? "production" : "development",
		entry: "./src/ts/Customizer.ts",
		optimization: {
			minimizer: [new UglifyJsPlugin()],
		},
		output: {
			filename: "customizer.bundle.js",
			path: BUILD_PATH,
			chunkFilename: "[name].chunk.js",
			publicPath: "src/"
		},
	
		// Enable sourcemaps for debugging webpack's output.
		devtool: "source-map",
	
		resolve: {
			// Add '.ts' and '.tsx' as resolvable extensions.
			extensions: [".ts", ".tsx", ".js", ".json"],
			plugins: [
				new TsConfigPathsPlugin(/* { tsconfig, compiler } */)
			]
		},
	
		module: {
			rules: [
	
				// All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
				// {
				// 	test: /\.ts$/,
				// 	include: path.join(__dirname, "src"),
				// 	loader: "ts-loader"
				// },
	
				
				// all except these extensions: .glsl .frag .vert
				{ test: /\.tsx?$/, loader: "awesome-typescript-loader" },
				{ 
					test: /\.(glsl|frag|vert)$/,
					exclude: /node_modules/,
					use: [
						'raw-loader',
						{
						  loader: 'glslify-loader',
						}
					]
				},
				// All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
				{ enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
			]
		},
	
		// When importing a module whose path matches one of the following, just
		// assume a corresponding global variable exists and use that instead.
		// This is important because it allows us to avoid bundling all of our
		// dependencies, which allows browsers to cache those libraries between builds.
		externals: {
		}
	}

	return config;
};
